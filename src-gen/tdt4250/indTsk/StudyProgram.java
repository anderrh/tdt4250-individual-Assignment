/**
 */
package tdt4250.indTsk;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Study Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.StudyProgram#getCode <em>Code</em>}</li>
 *   <li>{@link tdt4250.indTsk.StudyProgram#getConsistsOf <em>Consists Of</em>}</li>
 *   <li>{@link tdt4250.indTsk.StudyProgram#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.indTsk.StudyProgram#getStudents <em>Students</em>}</li>
 * </ul>
 *
 * @see tdt4250.indTsk.IndTskPackage#getStudyProgram()
 * @model
 * @generated
 */
public interface StudyProgram extends EObject {
	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see tdt4250.indTsk.IndTskPackage#getStudyProgram_Code()
	 * @model required="true"
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.StudyProgram#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Consists Of</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.indTsk.Course}.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.Course#getPartOf <em>Part Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Consists Of</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consists Of</em>' reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getStudyProgram_ConsistsOf()
	 * @see tdt4250.indTsk.Course#getPartOf
	 * @model opposite="partOf"
	 * @generated
	 */
	EList<Course> getConsistsOf();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.indTsk.IndTskPackage#getStudyProgram_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.StudyProgram#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Students</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.indTsk.Person}.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.Person#getRegisteredTo <em>Registered To</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Students</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Students</em>' reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getStudyProgram_Students()
	 * @see tdt4250.indTsk.Person#getRegisteredTo
	 * @model opposite="registeredTo"
	 * @generated
	 */
	EList<Person> getStudents();

} // StudyProgram

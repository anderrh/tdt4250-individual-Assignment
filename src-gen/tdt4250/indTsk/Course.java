/**
 */
package tdt4250.indTsk;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.Course#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.indTsk.Course#getCourseCode <em>Course Code</em>}</li>
 *   <li>{@link tdt4250.indTsk.Course#getContent <em>Content</em>}</li>
 *   <li>{@link tdt4250.indTsk.Course#getCourseinstance <em>Courseinstance</em>}</li>
 *   <li>{@link tdt4250.indTsk.Course#getPartOf <em>Part Of</em>}</li>
 *   <li>{@link tdt4250.indTsk.Course#getResponsibleDepartment <em>Responsible Department</em>}</li>
 *   <li>{@link tdt4250.indTsk.Course#getRequiresCourse <em>Requires Course</em>}</li>
 * </ul>
 *
 * @see tdt4250.indTsk.IndTskPackage#getCourse()
 * @model
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.indTsk.IndTskPackage#getCourse_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Course Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Code</em>' attribute.
	 * @see #setCourseCode(String)
	 * @see tdt4250.indTsk.IndTskPackage#getCourse_CourseCode()
	 * @model required="true"
	 * @generated
	 */
	String getCourseCode();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Course#getCourseCode <em>Course Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Code</em>' attribute.
	 * @see #getCourseCode()
	 * @generated
	 */
	void setCourseCode(String value);

	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(String)
	 * @see tdt4250.indTsk.IndTskPackage#getCourse_Content()
	 * @model required="true"
	 * @generated
	 */
	String getContent();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Course#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(String value);

	/**
	 * Returns the value of the '<em><b>Courseinstance</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.indTsk.CourseInstance}.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.CourseInstance#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Courseinstance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courseinstance</em>' containment reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getCourse_Courseinstance()
	 * @see tdt4250.indTsk.CourseInstance#getCourse
	 * @model opposite="course" containment="true"
	 * @generated
	 */
	EList<CourseInstance> getCourseinstance();

	/**
	 * Returns the value of the '<em><b>Part Of</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.indTsk.StudyProgram}.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.StudyProgram#getConsistsOf <em>Consists Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Part Of</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Part Of</em>' reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getCourse_PartOf()
	 * @see tdt4250.indTsk.StudyProgram#getConsistsOf
	 * @model opposite="consistsOf" required="true"
	 * @generated
	 */
	EList<StudyProgram> getPartOf();

	/**
	 * Returns the value of the '<em><b>Responsible Department</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.Department#getResponsibleFor <em>Responsible For</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible Department</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible Department</em>' reference.
	 * @see #setResponsibleDepartment(Department)
	 * @see tdt4250.indTsk.IndTskPackage#getCourse_ResponsibleDepartment()
	 * @see tdt4250.indTsk.Department#getResponsibleFor
	 * @model opposite="responsibleFor" required="true"
	 * @generated
	 */
	Department getResponsibleDepartment();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Course#getResponsibleDepartment <em>Responsible Department</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Responsible Department</em>' reference.
	 * @see #getResponsibleDepartment()
	 * @generated
	 */
	void setResponsibleDepartment(Department value);

	/**
	 * Returns the value of the '<em><b>Requires Course</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.indTsk.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requires Course</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requires Course</em>' reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getCourse_RequiresCourse()
	 * @model
	 * @generated
	 */
	EList<Course> getRequiresCourse();

} // Course

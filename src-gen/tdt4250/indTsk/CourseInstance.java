/**
 */
package tdt4250.indTsk;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.CourseInstance#getId <em>Id</em>}</li>
 *   <li>{@link tdt4250.indTsk.CourseInstance#getCredits <em>Credits</em>}</li>
 *   <li>{@link tdt4250.indTsk.CourseInstance#getMinLabHours <em>Min Lab Hours</em>}</li>
 *   <li>{@link tdt4250.indTsk.CourseInstance#getMinLecHours <em>Min Lec Hours</em>}</li>
 *   <li>{@link tdt4250.indTsk.CourseInstance#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.indTsk.CourseInstance#getEvaluationform <em>Evaluationform</em>}</li>
 *   <li>{@link tdt4250.indTsk.CourseInstance#getTimetable <em>Timetable</em>}</li>
 *   <li>{@link tdt4250.indTsk.CourseInstance#getRoles <em>Roles</em>}</li>
 *   <li>{@link tdt4250.indTsk.CourseInstance#getYear <em>Year</em>}</li>
 *   <li>{@link tdt4250.indTsk.CourseInstance#getSemesterType <em>Semester Type</em>}</li>
 * </ul>
 *
 * @see tdt4250.indTsk.IndTskPackage#getCourseInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='correctHourSum'"
 * @generated
 */
public interface CourseInstance extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see tdt4250.indTsk.IndTskPackage#getCourseInstance_Id()
	 * @model default="0"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.CourseInstance#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Credits</b></em>' attribute.
	 * The default value is <code>"7.5"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credits</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits</em>' attribute.
	 * @see #setCredits(double)
	 * @see tdt4250.indTsk.IndTskPackage#getCourseInstance_Credits()
	 * @model default="7.5" required="true"
	 * @generated
	 */
	double getCredits();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.CourseInstance#getCredits <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits</em>' attribute.
	 * @see #getCredits()
	 * @generated
	 */
	void setCredits(double value);

	/**
	 * Returns the value of the '<em><b>Min Lab Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Lab Hours</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Lab Hours</em>' attribute.
	 * @see #setMinLabHours(int)
	 * @see tdt4250.indTsk.IndTskPackage#getCourseInstance_MinLabHours()
	 * @model
	 * @generated
	 */
	int getMinLabHours();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.CourseInstance#getMinLabHours <em>Min Lab Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Lab Hours</em>' attribute.
	 * @see #getMinLabHours()
	 * @generated
	 */
	void setMinLabHours(int value);

	/**
	 * Returns the value of the '<em><b>Min Lec Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Lec Hours</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Lec Hours</em>' attribute.
	 * @see #setMinLecHours(int)
	 * @see tdt4250.indTsk.IndTskPackage#getCourseInstance_MinLecHours()
	 * @model
	 * @generated
	 */
	int getMinLecHours();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.CourseInstance#getMinLecHours <em>Min Lec Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Lec Hours</em>' attribute.
	 * @see #getMinLecHours()
	 * @generated
	 */
	void setMinLecHours(int value);

	/**
	 * Returns the value of the '<em><b>Course</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.Course#getCourseinstance <em>Courseinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' container reference.
	 * @see #setCourse(Course)
	 * @see tdt4250.indTsk.IndTskPackage#getCourseInstance_Course()
	 * @see tdt4250.indTsk.Course#getCourseinstance
	 * @model opposite="courseinstance" required="true" transient="false"
	 * @generated
	 */
	Course getCourse();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.CourseInstance#getCourse <em>Course</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' container reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(Course value);

	/**
	 * Returns the value of the '<em><b>Evaluationform</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.EvaluationForm#getCourseinstance <em>Courseinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluationform</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluationform</em>' containment reference.
	 * @see #setEvaluationform(EvaluationForm)
	 * @see tdt4250.indTsk.IndTskPackage#getCourseInstance_Evaluationform()
	 * @see tdt4250.indTsk.EvaluationForm#getCourseinstance
	 * @model opposite="courseinstance" containment="true" required="true"
	 * @generated
	 */
	EvaluationForm getEvaluationform();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.CourseInstance#getEvaluationform <em>Evaluationform</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Evaluationform</em>' containment reference.
	 * @see #getEvaluationform()
	 * @generated
	 */
	void setEvaluationform(EvaluationForm value);

	/**
	 * Returns the value of the '<em><b>Timetable</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.Timetable#getCourseinstance <em>Courseinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timetable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timetable</em>' containment reference.
	 * @see #setTimetable(Timetable)
	 * @see tdt4250.indTsk.IndTskPackage#getCourseInstance_Timetable()
	 * @see tdt4250.indTsk.Timetable#getCourseinstance
	 * @model opposite="courseinstance" containment="true" required="true"
	 * @generated
	 */
	Timetable getTimetable();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.CourseInstance#getTimetable <em>Timetable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timetable</em>' containment reference.
	 * @see #getTimetable()
	 * @generated
	 */
	void setTimetable(Timetable value);

	/**
	 * Returns the value of the '<em><b>Roles</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.indTsk.Roles}.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.Roles#getCourseinstance <em>Courseinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roles</em>' containment reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getCourseInstance_Roles()
	 * @see tdt4250.indTsk.Roles#getCourseinstance
	 * @model opposite="courseinstance" containment="true"
	 * @generated
	 */
	EList<Roles> getRoles();

	/**
	 * Returns the value of the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Year</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Year</em>' attribute.
	 * @see #setYear(int)
	 * @see tdt4250.indTsk.IndTskPackage#getCourseInstance_Year()
	 * @model
	 * @generated
	 */
	int getYear();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.CourseInstance#getYear <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Year</em>' attribute.
	 * @see #getYear()
	 * @generated
	 */
	void setYear(int value);

	/**
	 * Returns the value of the '<em><b>Semester Type</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.indTsk.SemesterType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Semester Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester Type</em>' attribute.
	 * @see tdt4250.indTsk.SemesterType
	 * @see #setSemesterType(SemesterType)
	 * @see tdt4250.indTsk.IndTskPackage#getCourseInstance_SemesterType()
	 * @model dataType="tdt4250.indTsk.SemesterType"
	 * @generated
	 */
	SemesterType getSemesterType();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.CourseInstance#getSemesterType <em>Semester Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester Type</em>' attribute.
	 * @see tdt4250.indTsk.SemesterType
	 * @see #getSemesterType()
	 * @generated
	 */
	void setSemesterType(SemesterType value);

} // CourseInstance

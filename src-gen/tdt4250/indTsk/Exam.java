/**
 */
package tdt4250.indTsk;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exam</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.Exam#isWritten <em>Written</em>}</li>
 *   <li>{@link tdt4250.indTsk.Exam#getDuration <em>Duration</em>}</li>
 * </ul>
 *
 * @see tdt4250.indTsk.IndTskPackage#getExam()
 * @model
 * @generated
 */
public interface Exam extends Activity {
	/**
	 * Returns the value of the '<em><b>Written</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Written</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Written</em>' attribute.
	 * @see #setWritten(boolean)
	 * @see tdt4250.indTsk.IndTskPackage#getExam_Written()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isWritten();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Exam#isWritten <em>Written</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Written</em>' attribute.
	 * @see #isWritten()
	 * @generated
	 */
	void setWritten(boolean value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(int)
	 * @see tdt4250.indTsk.IndTskPackage#getExam_Duration()
	 * @model
	 * @generated
	 */
	int getDuration();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Exam#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(int value);

} // Exam

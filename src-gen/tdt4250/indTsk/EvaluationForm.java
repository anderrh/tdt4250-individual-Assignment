/**
 */
package tdt4250.indTsk;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluation Form</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.EvaluationForm#getHasActivity <em>Has Activity</em>}</li>
 *   <li>{@link tdt4250.indTsk.EvaluationForm#getCourseinstance <em>Courseinstance</em>}</li>
 * </ul>
 *
 * @see tdt4250.indTsk.IndTskPackage#getEvaluationForm()
 * @model
 * @generated
 */
public interface EvaluationForm extends EObject {
	/**
	 * Returns the value of the '<em><b>Has Activity</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.indTsk.Activity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Activity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Activity</em>' containment reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getEvaluationForm_HasActivity()
	 * @model containment="true"
	 * @generated
	 */
	EList<Activity> getHasActivity();

	/**
	 * Returns the value of the '<em><b>Courseinstance</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.CourseInstance#getEvaluationform <em>Evaluationform</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Courseinstance</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courseinstance</em>' container reference.
	 * @see #setCourseinstance(CourseInstance)
	 * @see tdt4250.indTsk.IndTskPackage#getEvaluationForm_Courseinstance()
	 * @see tdt4250.indTsk.CourseInstance#getEvaluationform
	 * @model opposite="evaluationform" required="true" transient="false"
	 * @generated
	 */
	CourseInstance getCourseinstance();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.EvaluationForm#getCourseinstance <em>Courseinstance</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Courseinstance</em>' container reference.
	 * @see #getCourseinstance()
	 * @generated
	 */
	void setCourseinstance(CourseInstance value);

} // EvaluationForm

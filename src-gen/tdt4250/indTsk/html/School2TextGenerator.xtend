package tdt4250.indTsk.html

import tdt4250.indTsk.School
import tdt4250.indTsk.Course
import tdt4250.indTsk.CourseInstance
import org.eclipse.emf.common.util.EList
import tdt4250.indTsk.SemesterType
import tdt4250.indTsk.StudyProgram

class School2TextGenerator {
	var indentCount = 0
	private def String getIndent() {
		var retString = ""
		
		for(i : 0 ..< indentCount) {
			retString += "  "
		}
		retString
	}
	
	def String generateHtml(School school) {
		generateHtml(school, new StringBuilder).toString
	}
	
	def CharSequence generateHtml(School school, StringBuilder sBuilder) {
		//Generate prehtml, with some basic stylings
		applyPreHtml(school.name, sBuilder, '''
			p {
				margin-top: 5px;
			}
			
			h1, h2, h3, h4, h5, h6 {
				margin-bottom: 0px;
			}
			
			ul {
				margin: 0px;
			}
			
			.courseContainer {
				border: 1px solid #ccc!important;
				border-radius: 4px;
				margin: 10px;
				padding-left: 10px;
				display: flex;
			}
			
			.courseContainer::after {
			    content: "";
			    clear: both;
			    display: table;
			}
			
			.courseMainInfo {
				float: left;
				width: 74%;
			}
			
			.courseSideBar {
				border: 1px solid #ccc!important;
				border-radius: 4px;
				float: right;
				width: 25%;
				padding: 5px;
			}
			
			.courseSubtitle {
				margin-top: 0px;
			}
		''')
		
		// Generate a page header
		genereateHeader("Velkommen til " + school.name, 1, sBuilder)

		// Generate page of courses
		school.courses.forEach[generateCourseElement(it, sBuilder)]
		
		
		 
		
		applyPostHtml(sBuilder)
	}
	
	def generateCourseElement(Course course, StringBuilder sBuilder) {
		sBuilder << '''
			�indent�<div class=courseContainer>
			�indent�  <div class=courseMainInfo>
			�indent�    <h3>�course.courseCode� - �course.name�</h3>
			�indent�    <h5 class="courseSubtitle">�course.responsibleDepartment.name�</h5>
			�indent�    <p>�course.content� </p>
		'''
		
		indentCount += 2
		
		generateCourseTabs(course.courseinstance, sBuilder)
			
		indentCount --
		sBuilder << '''
			�indent�</div>
		'''
		
		generateCourseInfoSidebar(course, sBuilder)
		
		indentCount --
		sBuilder <<	'''
			�indent�</div>
		'''
	}
	
	def generateCourseTabs(EList<CourseInstance> instances, StringBuilder builder) {
		 indentCount ++
		 val style = '''
			�indent�.courseTab {
			�indent�    overflow: hidden;
			�indent�    border: 1px solid #ccc;
			�indent�    background-color: #f1f1f1;
			�indent�}
			
			�indent�/* Style the buttons that are used to open the tab content */
			�indent�.courseTab button {
			�indent�    background-color: inherit;
			�indent�    float: left;
			�indent�    border: none;
			�indent�    outline: none;
			�indent�    cursor: pointer;
			�indent�    padding: 14px 16px;
			�indent�    transition: 0.3s;
			�indent�}
			
			�indent�/* Change background color of buttons on hover */
			�indent�.courseTab button:hover {
			�indent�    background-color: #ddd;
			�indent�}
			
			�indent�/* Create an active/current tablink class */
			�indent�.courseTab button.active {
			�indent�    background-color: #ccc;
			�indent�}
			
			�indent�/* Style the tab content */
			�indent�.courseTabContent {
			�indent�    display: none;
			�indent�    padding: 6px 12px;
			�indent�    border: 1px solid #ccc;
			�indent�    border-top: none;
			�indent�} 
		'''
		val script = '''
			�indent�function openInstance(evt, instanceId) {
			�indent�	// Declare all variables
			�indent�	var i, tabcontent, tablinks;
			
			�indent�	// Get all elements with class="courseTabContent" and hide them
			�indent�	tabcontent = document.getElementsByClassName("courseTabContent");
			�indent�	for (i = 0; i < tabcontent.length; i++) {
			�indent�		tabcontent[i].style.display = "none";
			�indent�	}
			
			�indent�	// Get all elements with class="tablinks" and remove the class "active"
			�indent�	tablinks = document.getElementsByClassName("tablinks");
			�indent�	for (i = 0; i < tablinks.length; i++) {
			�indent�		tablinks[i].className = tablinks[i].className.replace(" active", "");
			�indent�	}
			
			�indent�	// Show the current tab, and add an "active" class to the button that opened the tab
			�indent�	document.getElementById(instanceId).style.display = "block";
			�indent�	evt.currentTarget.className += " active";
			�indent�}
		'''
		indentCount --;
		
		generateStyle(style, builder)
		generateScript(script, builder)
		builder << '''
			�indent�<div class=courseTab>
		'''
		indentCount ++
		instances.forEach[generateCourseTabButtons(it, builder)]
		indentCount --
		builder << '''
			�indent�</div>
		'''
		instances.forEach[generateCourseTabContent(it, builder)]
		
	}
	
	def generateScript(String script, StringBuilder builder) {
		builder << '''
			�indent�<script>
			�script�
			�indent�</script>
		'''
	}
	
	def generateStyle(String style, StringBuilder builder) {
		builder << '''
			�indent�<style>
			�style�
			�indent�</style>
		'''
	}
	
	def generateCourseTabContent(CourseInstance instance, StringBuilder builder) {
		var semesterType = ""
		if (instance.semesterType.value == SemesterType.AUTUMN_VALUE) {
			semesterType = "H�st"
		} else {
			semesterType = "V�r"
		}
		
		builder << '''
			�indent�<div id=�instance.course.courseCode�.�instance.year� class=courseTabContent>
			�indent�  <p>Semester: �semesterType�</p>
			�indent�  <p>Poeng: �instance.credits�</p>
			�indent�  <p>Labtimer: �instance.timetable.totalLabHours�</p>
			�indent�  <p>Undervisningstimer: �instance.timetable.totalLecHours�</p>
			�indent�</div>
		'''
	}
	
	def generateCourseTabButtons(CourseInstance instance, StringBuilder builder) {
		builder << '''
			�indent�<button class="tablinks" onclick="openInstance(event, '�instance.course.courseCode�.�instance.year�')">�instance.year�</button>
		'''
	}
	
	
	def generateCourseInfoSidebar(Course course, StringBuilder sBuilder) {
		sBuilder << '''
			�indent�<div class="courseSideBar">
			�indent�  <h4>Studielinjer:</h4>
			�indent�  <ul>
		'''
		
		indentCount += 2
		course.partOf.forEach[generateCourseStudyPrograms(it, sBuilder)]
		indentCount -= 2
		
		sBuilder << '''	
			�indent�  </ul>
		'''
		if (course.requiresCourse.size > 0) {
		sBuilder << '''
				�indent�  <h4>Krever fag:</h4>
				�indent�  <ul>
			'''
			indentCount += 2
			course.requiresCourse.forEach[generateRequiredCourses(it, sBuilder)]
			indentCount -= 2
		
		sBuilder << '''	
				�indent�  </ul>
			'''
		}		
		sBuilder << '''	
			�indent�</div>
		'''
		
	}
	
	def generateRequiredCourses(Course course, StringBuilder sBuilder) {
		sBuilder << '''
			�indent�<li>�course.courseCode� - �course.name�</li>
		'''
	}
	
	def generateCourseStudyPrograms(StudyProgram program, StringBuilder sBuilder) {
		sBuilder << '''
			�indent�<li>�program.code� - �program.name�</li>
		'''
	}
	
	/* �� */
	def genereateHeader(String headerText, int headerSize, StringBuilder sBuilder) {
		val indent = getIndent()
		sBuilder << '''
			�indent�<h�headerSize�>�headerText�</h�headerSize�>
		'''
	}
	
	def CharSequence applyPreHtml(String title, StringBuilder sBuilder) {
		indentCount += 2
		sBuilder << '''
			<html>
			  <head>
			    <title>�title�</title>
			    <meta charset="utf-8"/>
			  </head>
			  <body>
		'''
	}
	def CharSequence applyPreHtml(String title, StringBuilder sBuilder, String style) {
		indentCount += 2
		sBuilder << '''
			<html>
			  <head>
			    <title>�title�</title>
			    <meta charset="utf-8"/>
			  </head>
			  <style>�style�</style>
			  <body>
		'''
	}
	def CharSequence applyPreHtml(String title, StringBuilder sBuilder, String style, String script) {
		indentCount += 2
		sBuilder << '''
			<html>
			  <head>
			    <title>�title�</title>
			    <meta charset="utf-8"/>
			  </head>
			  <style>�style�</style>
			  <script>�script�</script>
			  <body>
		'''
	}
	
	def CharSequence applyPostHtml(StringBuilder sBuilder) {
		indentCount -= 2
		sBuilder << '''
			  </body>
			</html>
		'''
	}

	// << operator
	def static StringBuilder operator_doubleLessThan(StringBuilder stringBuilder, Object o) {
		return stringBuilder.append(o);
	}
}

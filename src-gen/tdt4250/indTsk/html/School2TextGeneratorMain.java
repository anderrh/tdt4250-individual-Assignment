package tdt4250.indTsk.html;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import tdt4250.indTsk.IndTskPackage;
import tdt4250.indTsk.School;
import tdt4250.indTsk.util.IndTskResourceFactoryImpl;



public class School2TextGeneratorMain {
	
	public static void main(String[] args) throws IOException {
		School school = (args.length > 0 ? School2TextGeneratorMain.getSchool(args[0]) : School2TextGeneratorMain.getSampleSchool());
		String html = new School2TextGenerator().generateHtml(school);
		if (args.length > 1) {
			URI target = URI.createURI(args[0]);
			try (PrintStream ps = new PrintStream(school.eResource().getResourceSet().getURIConverter().createOutputStream(target))){
				ps.print(html);
			}
		} else {
			try (PrintWriter fileOut = new PrintWriter("src-gen\\tdt4250\\indTsk\\html\\school.html")){
				fileOut.print(html);
			}
			
			System.out.println(html);
		}	
	}
	
	public static School getSchool(String uriString) throws IOException {
		ResourceSet resSet = new ResourceSetImpl();
		resSet.getPackageRegistry().put(
				IndTskPackage.eNS_URI,
				IndTskPackage.eINSTANCE);
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
				"xmi",
				new IndTskResourceFactoryImpl());
		Resource resource = resSet.getResource(URI.createURI(uriString), true);
		for (EObject eObject : resource.getContents()) {
			if (eObject instanceof School) {
				return (School) eObject;
			}
		}
		return null;
	}

	public static School getSampleSchool() {
		try {
			return getSchool(
					School2TextGeneratorMain.class.getResource("SampleSchool.xmi").toString()
					);
		} catch (IOException e) {
			System.err.println(e);
			return null;
		}
	}
}

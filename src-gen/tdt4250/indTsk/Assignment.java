/**
 */
package tdt4250.indTsk;

import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.Assignment#getDeadline <em>Deadline</em>}</li>
 * </ul>
 *
 * @see tdt4250.indTsk.IndTskPackage#getAssignment()
 * @model
 * @generated
 */
public interface Assignment extends Activity {
	/**
	 * Returns the value of the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deadline</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deadline</em>' attribute.
	 * @see #setDeadline(Date)
	 * @see tdt4250.indTsk.IndTskPackage#getAssignment_Deadline()
	 * @model
	 * @generated
	 */
	Date getDeadline();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Assignment#getDeadline <em>Deadline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deadline</em>' attribute.
	 * @see #getDeadline()
	 * @generated
	 */
	void setDeadline(Date value);

} // Assignment

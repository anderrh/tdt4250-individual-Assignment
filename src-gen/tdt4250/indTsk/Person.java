/**
 */
package tdt4250.indTsk;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.Person#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.indTsk.Person#getRoles <em>Roles</em>}</li>
 *   <li>{@link tdt4250.indTsk.Person#getEmployedAt <em>Employed At</em>}</li>
 *   <li>{@link tdt4250.indTsk.Person#getRegisteredTo <em>Registered To</em>}</li>
 *   <li>{@link tdt4250.indTsk.Person#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see tdt4250.indTsk.IndTskPackage#getPerson()
 * @model
 * @generated
 */
public interface Person extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.indTsk.IndTskPackage#getPerson_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Person#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Roles</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.indTsk.Roles}.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.Roles#getPerson <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roles</em>' reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getPerson_Roles()
	 * @see tdt4250.indTsk.Roles#getPerson
	 * @model opposite="person"
	 * @generated
	 */
	EList<Roles> getRoles();

	/**
	 * Returns the value of the '<em><b>Employed At</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.indTsk.Department}.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.Department#getEmployees <em>Employees</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Employed At</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Employed At</em>' reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getPerson_EmployedAt()
	 * @see tdt4250.indTsk.Department#getEmployees
	 * @model opposite="employees"
	 * @generated
	 */
	EList<Department> getEmployedAt();

	/**
	 * Returns the value of the '<em><b>Registered To</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.indTsk.StudyProgram}.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.StudyProgram#getStudents <em>Students</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered To</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered To</em>' reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getPerson_RegisteredTo()
	 * @see tdt4250.indTsk.StudyProgram#getStudents
	 * @model opposite="students"
	 * @generated
	 */
	EList<StudyProgram> getRegisteredTo();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see tdt4250.indTsk.IndTskPackage#getPerson_Id()
	 * @model required="true" derived="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Person#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

} // Person

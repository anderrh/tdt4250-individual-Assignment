/**
 */
package tdt4250.indTsk;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Roles</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.Roles#getPerson <em>Person</em>}</li>
 *   <li>{@link tdt4250.indTsk.Roles#getCourseinstance <em>Courseinstance</em>}</li>
 *   <li>{@link tdt4250.indTsk.Roles#getRoleType <em>Role Type</em>}</li>
 * </ul>
 *
 * @see tdt4250.indTsk.IndTskPackage#getRoles()
 * @model
 * @generated
 */
public interface Roles extends EObject {
	/**
	 * Returns the value of the '<em><b>Person</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.indTsk.Person}.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.Person#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person</em>' reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getRoles_Person()
	 * @see tdt4250.indTsk.Person#getRoles
	 * @model opposite="roles"
	 * @generated
	 */
	EList<Person> getPerson();

	/**
	 * Returns the value of the '<em><b>Courseinstance</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.CourseInstance#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Courseinstance</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courseinstance</em>' container reference.
	 * @see #setCourseinstance(CourseInstance)
	 * @see tdt4250.indTsk.IndTskPackage#getRoles_Courseinstance()
	 * @see tdt4250.indTsk.CourseInstance#getRoles
	 * @model opposite="roles" transient="false"
	 * @generated
	 */
	CourseInstance getCourseinstance();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Roles#getCourseinstance <em>Courseinstance</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Courseinstance</em>' container reference.
	 * @see #getCourseinstance()
	 * @generated
	 */
	void setCourseinstance(CourseInstance value);

	/**
	 * Returns the value of the '<em><b>Role Type</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.indTsk.RoleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Type</em>' attribute.
	 * @see tdt4250.indTsk.RoleType
	 * @see #setRoleType(RoleType)
	 * @see tdt4250.indTsk.IndTskPackage#getRoles_RoleType()
	 * @model
	 * @generated
	 */
	RoleType getRoleType();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Roles#getRoleType <em>Role Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role Type</em>' attribute.
	 * @see tdt4250.indTsk.RoleType
	 * @see #getRoleType()
	 * @generated
	 */
	void setRoleType(RoleType value);

} // Roles

/**
 */
package tdt4250.indTsk;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see tdt4250.indTsk.IndTskPackage#getProject()
 * @model
 * @generated
 */
public interface Project extends Activity {
} // Project

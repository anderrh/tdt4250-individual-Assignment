/**
 */
package tdt4250.indTsk;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reservation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.Reservation#getDay <em>Day</em>}</li>
 *   <li>{@link tdt4250.indTsk.Reservation#getReservationType <em>Reservation Type</em>}</li>
 *   <li>{@link tdt4250.indTsk.Reservation#getFromHour <em>From Hour</em>}</li>
 *   <li>{@link tdt4250.indTsk.Reservation#getToHour <em>To Hour</em>}</li>
 *   <li>{@link tdt4250.indTsk.Reservation#getReservedRoom <em>Reserved Room</em>}</li>
 * </ul>
 *
 * @see tdt4250.indTsk.IndTskPackage#getReservation()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='fromLowerThanTo'"
 * @generated
 */
public interface Reservation extends EObject {
	/**
	 * Returns the value of the '<em><b>Day</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.indTsk.Day}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Day</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Day</em>' attribute.
	 * @see tdt4250.indTsk.Day
	 * @see #setDay(Day)
	 * @see tdt4250.indTsk.IndTskPackage#getReservation_Day()
	 * @model dataType="tdt4250.indTsk.Day"
	 * @generated
	 */
	Day getDay();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Reservation#getDay <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Day</em>' attribute.
	 * @see tdt4250.indTsk.Day
	 * @see #getDay()
	 * @generated
	 */
	void setDay(Day value);

	/**
	 * Returns the value of the '<em><b>Reservation Type</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.indTsk.ReservationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reservation Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reservation Type</em>' attribute.
	 * @see tdt4250.indTsk.ReservationType
	 * @see #setReservationType(ReservationType)
	 * @see tdt4250.indTsk.IndTskPackage#getReservation_ReservationType()
	 * @model
	 * @generated
	 */
	ReservationType getReservationType();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Reservation#getReservationType <em>Reservation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reservation Type</em>' attribute.
	 * @see tdt4250.indTsk.ReservationType
	 * @see #getReservationType()
	 * @generated
	 */
	void setReservationType(ReservationType value);

	/**
	 * Returns the value of the '<em><b>From Hour</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Hour</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Hour</em>' attribute.
	 * @see #setFromHour(int)
	 * @see tdt4250.indTsk.IndTskPackage#getReservation_FromHour()
	 * @model
	 * @generated
	 */
	int getFromHour();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Reservation#getFromHour <em>From Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Hour</em>' attribute.
	 * @see #getFromHour()
	 * @generated
	 */
	void setFromHour(int value);

	/**
	 * Returns the value of the '<em><b>To Hour</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Hour</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Hour</em>' attribute.
	 * @see #setToHour(int)
	 * @see tdt4250.indTsk.IndTskPackage#getReservation_ToHour()
	 * @model
	 * @generated
	 */
	int getToHour();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Reservation#getToHour <em>To Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Hour</em>' attribute.
	 * @see #getToHour()
	 * @generated
	 */
	void setToHour(int value);

	/**
	 * Returns the value of the '<em><b>Reserved Room</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.Room#getReservedTo <em>Reserved To</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reserved Room</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reserved Room</em>' reference.
	 * @see #setReservedRoom(Room)
	 * @see tdt4250.indTsk.IndTskPackage#getReservation_ReservedRoom()
	 * @see tdt4250.indTsk.Room#getReservedTo
	 * @model opposite="reservedTo"
	 * @generated
	 */
	Room getReservedRoom();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Reservation#getReservedRoom <em>Reserved Room</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reserved Room</em>' reference.
	 * @see #getReservedRoom()
	 * @generated
	 */
	void setReservedRoom(Room value);

} // Reservation

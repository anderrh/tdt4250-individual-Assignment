/**
 */
package tdt4250.indTsk.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.indTsk.Course;
import tdt4250.indTsk.Department;
import tdt4250.indTsk.IndTskPackage;
import tdt4250.indTsk.Person;
import tdt4250.indTsk.Room;
import tdt4250.indTsk.School;
import tdt4250.indTsk.StudyProgram;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>School</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.impl.SchoolImpl#getCourses <em>Courses</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.SchoolImpl#getStudyprograms <em>Studyprograms</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.SchoolImpl#getPeople <em>People</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.SchoolImpl#getDepartments <em>Departments</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.SchoolImpl#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.SchoolImpl#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SchoolImpl extends MinimalEObjectImpl.Container implements School {
	/**
	 * The cached value of the '{@link #getCourses() <em>Courses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> courses;

	/**
	 * The cached value of the '{@link #getStudyprograms() <em>Studyprograms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyprograms()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> studyprograms;

	/**
	 * The cached value of the '{@link #getPeople() <em>People</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeople()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> people;

	/**
	 * The cached value of the '{@link #getDepartments() <em>Departments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepartments()
	 * @generated
	 * @ordered
	 */
	protected EList<Department> departments;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> rooms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SchoolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IndTskPackage.Literals.SCHOOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getCourses() {
		if (courses == null) {
			courses = new EObjectContainmentEList<Course>(Course.class, this, IndTskPackage.SCHOOL__COURSES);
		}
		return courses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getStudyprograms() {
		if (studyprograms == null) {
			studyprograms = new EObjectContainmentEList<StudyProgram>(StudyProgram.class, this,
					IndTskPackage.SCHOOL__STUDYPROGRAMS);
		}
		return studyprograms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Person> getPeople() {
		if (people == null) {
			people = new EObjectContainmentEList<Person>(Person.class, this, IndTskPackage.SCHOOL__PEOPLE);
		}
		return people;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Department> getDepartments() {
		if (departments == null) {
			departments = new EObjectContainmentEList<Department>(Department.class, this,
					IndTskPackage.SCHOOL__DEPARTMENTS);
		}
		return departments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.SCHOOL__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Room> getRooms() {
		if (rooms == null) {
			rooms = new EObjectContainmentEList<Room>(Room.class, this, IndTskPackage.SCHOOL__ROOMS);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.SCHOOL__COURSES:
			return ((InternalEList<?>) getCourses()).basicRemove(otherEnd, msgs);
		case IndTskPackage.SCHOOL__STUDYPROGRAMS:
			return ((InternalEList<?>) getStudyprograms()).basicRemove(otherEnd, msgs);
		case IndTskPackage.SCHOOL__PEOPLE:
			return ((InternalEList<?>) getPeople()).basicRemove(otherEnd, msgs);
		case IndTskPackage.SCHOOL__DEPARTMENTS:
			return ((InternalEList<?>) getDepartments()).basicRemove(otherEnd, msgs);
		case IndTskPackage.SCHOOL__ROOMS:
			return ((InternalEList<?>) getRooms()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IndTskPackage.SCHOOL__COURSES:
			return getCourses();
		case IndTskPackage.SCHOOL__STUDYPROGRAMS:
			return getStudyprograms();
		case IndTskPackage.SCHOOL__PEOPLE:
			return getPeople();
		case IndTskPackage.SCHOOL__DEPARTMENTS:
			return getDepartments();
		case IndTskPackage.SCHOOL__NAME:
			return getName();
		case IndTskPackage.SCHOOL__ROOMS:
			return getRooms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IndTskPackage.SCHOOL__COURSES:
			getCourses().clear();
			getCourses().addAll((Collection<? extends Course>) newValue);
			return;
		case IndTskPackage.SCHOOL__STUDYPROGRAMS:
			getStudyprograms().clear();
			getStudyprograms().addAll((Collection<? extends StudyProgram>) newValue);
			return;
		case IndTskPackage.SCHOOL__PEOPLE:
			getPeople().clear();
			getPeople().addAll((Collection<? extends Person>) newValue);
			return;
		case IndTskPackage.SCHOOL__DEPARTMENTS:
			getDepartments().clear();
			getDepartments().addAll((Collection<? extends Department>) newValue);
			return;
		case IndTskPackage.SCHOOL__NAME:
			setName((String) newValue);
			return;
		case IndTskPackage.SCHOOL__ROOMS:
			getRooms().clear();
			getRooms().addAll((Collection<? extends Room>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IndTskPackage.SCHOOL__COURSES:
			getCourses().clear();
			return;
		case IndTskPackage.SCHOOL__STUDYPROGRAMS:
			getStudyprograms().clear();
			return;
		case IndTskPackage.SCHOOL__PEOPLE:
			getPeople().clear();
			return;
		case IndTskPackage.SCHOOL__DEPARTMENTS:
			getDepartments().clear();
			return;
		case IndTskPackage.SCHOOL__NAME:
			setName(NAME_EDEFAULT);
			return;
		case IndTskPackage.SCHOOL__ROOMS:
			getRooms().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IndTskPackage.SCHOOL__COURSES:
			return courses != null && !courses.isEmpty();
		case IndTskPackage.SCHOOL__STUDYPROGRAMS:
			return studyprograms != null && !studyprograms.isEmpty();
		case IndTskPackage.SCHOOL__PEOPLE:
			return people != null && !people.isEmpty();
		case IndTskPackage.SCHOOL__DEPARTMENTS:
			return departments != null && !departments.isEmpty();
		case IndTskPackage.SCHOOL__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case IndTskPackage.SCHOOL__ROOMS:
			return rooms != null && !rooms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //SchoolImpl

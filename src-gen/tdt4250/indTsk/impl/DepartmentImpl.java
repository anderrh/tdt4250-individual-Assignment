/**
 */
package tdt4250.indTsk.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.indTsk.Course;
import tdt4250.indTsk.Department;
import tdt4250.indTsk.IndTskPackage;
import tdt4250.indTsk.Person;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.impl.DepartmentImpl#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.DepartmentImpl#getResponsibleFor <em>Responsible For</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.DepartmentImpl#getEmployees <em>Employees</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DepartmentImpl extends MinimalEObjectImpl.Container implements Department {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getResponsibleFor() <em>Responsible For</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibleFor()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> responsibleFor;

	/**
	 * The cached value of the '{@link #getEmployees() <em>Employees</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmployees()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> employees;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DepartmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IndTskPackage.Literals.DEPARTMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.DEPARTMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getResponsibleFor() {
		if (responsibleFor == null) {
			responsibleFor = new EObjectWithInverseResolvingEList<Course>(Course.class, this,
					IndTskPackage.DEPARTMENT__RESPONSIBLE_FOR, IndTskPackage.COURSE__RESPONSIBLE_DEPARTMENT);
		}
		return responsibleFor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Person> getEmployees() {
		if (employees == null) {
			employees = new EObjectWithInverseResolvingEList.ManyInverse<Person>(Person.class, this,
					IndTskPackage.DEPARTMENT__EMPLOYEES, IndTskPackage.PERSON__EMPLOYED_AT);
		}
		return employees;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.DEPARTMENT__RESPONSIBLE_FOR:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getResponsibleFor()).basicAdd(otherEnd, msgs);
		case IndTskPackage.DEPARTMENT__EMPLOYEES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getEmployees()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.DEPARTMENT__RESPONSIBLE_FOR:
			return ((InternalEList<?>) getResponsibleFor()).basicRemove(otherEnd, msgs);
		case IndTskPackage.DEPARTMENT__EMPLOYEES:
			return ((InternalEList<?>) getEmployees()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IndTskPackage.DEPARTMENT__NAME:
			return getName();
		case IndTskPackage.DEPARTMENT__RESPONSIBLE_FOR:
			return getResponsibleFor();
		case IndTskPackage.DEPARTMENT__EMPLOYEES:
			return getEmployees();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IndTskPackage.DEPARTMENT__NAME:
			setName((String) newValue);
			return;
		case IndTskPackage.DEPARTMENT__RESPONSIBLE_FOR:
			getResponsibleFor().clear();
			getResponsibleFor().addAll((Collection<? extends Course>) newValue);
			return;
		case IndTskPackage.DEPARTMENT__EMPLOYEES:
			getEmployees().clear();
			getEmployees().addAll((Collection<? extends Person>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IndTskPackage.DEPARTMENT__NAME:
			setName(NAME_EDEFAULT);
			return;
		case IndTskPackage.DEPARTMENT__RESPONSIBLE_FOR:
			getResponsibleFor().clear();
			return;
		case IndTskPackage.DEPARTMENT__EMPLOYEES:
			getEmployees().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IndTskPackage.DEPARTMENT__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case IndTskPackage.DEPARTMENT__RESPONSIBLE_FOR:
			return responsibleFor != null && !responsibleFor.isEmpty();
		case IndTskPackage.DEPARTMENT__EMPLOYEES:
			return employees != null && !employees.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //DepartmentImpl

/**
 */
package tdt4250.indTsk.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.indTsk.Course;
import tdt4250.indTsk.CourseInstance;
import tdt4250.indTsk.EvaluationForm;
import tdt4250.indTsk.IndTskPackage;
import tdt4250.indTsk.Roles;
import tdt4250.indTsk.SemesterType;
import tdt4250.indTsk.Timetable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.impl.CourseInstanceImpl#getId <em>Id</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseInstanceImpl#getCredits <em>Credits</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseInstanceImpl#getMinLabHours <em>Min Lab Hours</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseInstanceImpl#getMinLecHours <em>Min Lec Hours</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseInstanceImpl#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseInstanceImpl#getEvaluationform <em>Evaluationform</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseInstanceImpl#getTimetable <em>Timetable</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseInstanceImpl#getRoles <em>Roles</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseInstanceImpl#getYear <em>Year</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseInstanceImpl#getSemesterType <em>Semester Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseInstanceImpl extends MinimalEObjectImpl.Container implements CourseInstance {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected static final double CREDITS_EDEFAULT = 7.5;

	/**
	 * The cached value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected double credits = CREDITS_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinLabHours() <em>Min Lab Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinLabHours()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_LAB_HOURS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinLabHours() <em>Min Lab Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinLabHours()
	 * @generated
	 * @ordered
	 */
	protected int minLabHours = MIN_LAB_HOURS_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinLecHours() <em>Min Lec Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinLecHours()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_LEC_HOURS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinLecHours() <em>Min Lec Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinLecHours()
	 * @generated
	 * @ordered
	 */
	protected int minLecHours = MIN_LEC_HOURS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEvaluationform() <em>Evaluationform</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluationform()
	 * @generated
	 * @ordered
	 */
	protected EvaluationForm evaluationform;

	/**
	 * The cached value of the '{@link #getTimetable() <em>Timetable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimetable()
	 * @generated
	 * @ordered
	 */
	protected Timetable timetable;

	/**
	 * The cached value of the '{@link #getRoles() <em>Roles</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoles()
	 * @generated
	 * @ordered
	 */
	protected EList<Roles> roles;

	/**
	 * The default value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected static final int YEAR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected int year = YEAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getSemesterType() <em>Semester Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterType()
	 * @generated
	 * @ordered
	 */
	protected static final SemesterType SEMESTER_TYPE_EDEFAULT = SemesterType.AUTUMN;

	/**
	 * The cached value of the '{@link #getSemesterType() <em>Semester Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterType()
	 * @generated
	 * @ordered
	 */
	protected SemesterType semesterType = SEMESTER_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IndTskPackage.Literals.COURSE_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.COURSE_INSTANCE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCredits() {
		return credits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCredits(double newCredits) {
		double oldCredits = credits;
		credits = newCredits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.COURSE_INSTANCE__CREDITS, oldCredits,
					credits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinLabHours() {
		return minLabHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinLabHours(int newMinLabHours) {
		int oldMinLabHours = minLabHours;
		minLabHours = newMinLabHours;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.COURSE_INSTANCE__MIN_LAB_HOURS,
					oldMinLabHours, minLabHours));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinLecHours() {
		return minLecHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinLecHours(int newMinLecHours) {
		int oldMinLecHours = minLecHours;
		minLecHours = newMinLecHours;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.COURSE_INSTANCE__MIN_LEC_HOURS,
					oldMinLecHours, minLecHours));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getCourse() {
		if (eContainerFeatureID() != IndTskPackage.COURSE_INSTANCE__COURSE)
			return null;
		return (Course) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourse(Course newCourse, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCourse, IndTskPackage.COURSE_INSTANCE__COURSE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourse(Course newCourse) {
		if (newCourse != eInternalContainer()
				|| (eContainerFeatureID() != IndTskPackage.COURSE_INSTANCE__COURSE && newCourse != null)) {
			if (EcoreUtil.isAncestor(this, newCourse))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourse != null)
				msgs = ((InternalEObject) newCourse).eInverseAdd(this, IndTskPackage.COURSE__COURSEINSTANCE,
						Course.class, msgs);
			msgs = basicSetCourse(newCourse, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.COURSE_INSTANCE__COURSE, newCourse,
					newCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationForm getEvaluationform() {
		return evaluationform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEvaluationform(EvaluationForm newEvaluationform, NotificationChain msgs) {
		EvaluationForm oldEvaluationform = evaluationform;
		evaluationform = newEvaluationform;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					IndTskPackage.COURSE_INSTANCE__EVALUATIONFORM, oldEvaluationform, newEvaluationform);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvaluationform(EvaluationForm newEvaluationform) {
		if (newEvaluationform != evaluationform) {
			NotificationChain msgs = null;
			if (evaluationform != null)
				msgs = ((InternalEObject) evaluationform).eInverseRemove(this,
						IndTskPackage.EVALUATION_FORM__COURSEINSTANCE, EvaluationForm.class, msgs);
			if (newEvaluationform != null)
				msgs = ((InternalEObject) newEvaluationform).eInverseAdd(this,
						IndTskPackage.EVALUATION_FORM__COURSEINSTANCE, EvaluationForm.class, msgs);
			msgs = basicSetEvaluationform(newEvaluationform, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.COURSE_INSTANCE__EVALUATIONFORM,
					newEvaluationform, newEvaluationform));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timetable getTimetable() {
		return timetable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTimetable(Timetable newTimetable, NotificationChain msgs) {
		Timetable oldTimetable = timetable;
		timetable = newTimetable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					IndTskPackage.COURSE_INSTANCE__TIMETABLE, oldTimetable, newTimetable);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimetable(Timetable newTimetable) {
		if (newTimetable != timetable) {
			NotificationChain msgs = null;
			if (timetable != null)
				msgs = ((InternalEObject) timetable).eInverseRemove(this, IndTskPackage.TIMETABLE__COURSEINSTANCE,
						Timetable.class, msgs);
			if (newTimetable != null)
				msgs = ((InternalEObject) newTimetable).eInverseAdd(this, IndTskPackage.TIMETABLE__COURSEINSTANCE,
						Timetable.class, msgs);
			msgs = basicSetTimetable(newTimetable, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.COURSE_INSTANCE__TIMETABLE,
					newTimetable, newTimetable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Roles> getRoles() {
		if (roles == null) {
			roles = new EObjectContainmentWithInverseEList<Roles>(Roles.class, this,
					IndTskPackage.COURSE_INSTANCE__ROLES, IndTskPackage.ROLES__COURSEINSTANCE);
		}
		return roles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getYear() {
		return year;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setYear(int newYear) {
		int oldYear = year;
		year = newYear;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.COURSE_INSTANCE__YEAR, oldYear, year));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemesterType getSemesterType() {
		return semesterType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSemesterType(SemesterType newSemesterType) {
		SemesterType oldSemesterType = semesterType;
		semesterType = newSemesterType == null ? SEMESTER_TYPE_EDEFAULT : newSemesterType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.COURSE_INSTANCE__SEMESTER_TYPE,
					oldSemesterType, semesterType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.COURSE_INSTANCE__COURSE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCourse((Course) otherEnd, msgs);
		case IndTskPackage.COURSE_INSTANCE__EVALUATIONFORM:
			if (evaluationform != null)
				msgs = ((InternalEObject) evaluationform).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - IndTskPackage.COURSE_INSTANCE__EVALUATIONFORM, null, msgs);
			return basicSetEvaluationform((EvaluationForm) otherEnd, msgs);
		case IndTskPackage.COURSE_INSTANCE__TIMETABLE:
			if (timetable != null)
				msgs = ((InternalEObject) timetable).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - IndTskPackage.COURSE_INSTANCE__TIMETABLE, null, msgs);
			return basicSetTimetable((Timetable) otherEnd, msgs);
		case IndTskPackage.COURSE_INSTANCE__ROLES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getRoles()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.COURSE_INSTANCE__COURSE:
			return basicSetCourse(null, msgs);
		case IndTskPackage.COURSE_INSTANCE__EVALUATIONFORM:
			return basicSetEvaluationform(null, msgs);
		case IndTskPackage.COURSE_INSTANCE__TIMETABLE:
			return basicSetTimetable(null, msgs);
		case IndTskPackage.COURSE_INSTANCE__ROLES:
			return ((InternalEList<?>) getRoles()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case IndTskPackage.COURSE_INSTANCE__COURSE:
			return eInternalContainer().eInverseRemove(this, IndTskPackage.COURSE__COURSEINSTANCE, Course.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IndTskPackage.COURSE_INSTANCE__ID:
			return getId();
		case IndTskPackage.COURSE_INSTANCE__CREDITS:
			return getCredits();
		case IndTskPackage.COURSE_INSTANCE__MIN_LAB_HOURS:
			return getMinLabHours();
		case IndTskPackage.COURSE_INSTANCE__MIN_LEC_HOURS:
			return getMinLecHours();
		case IndTskPackage.COURSE_INSTANCE__COURSE:
			return getCourse();
		case IndTskPackage.COURSE_INSTANCE__EVALUATIONFORM:
			return getEvaluationform();
		case IndTskPackage.COURSE_INSTANCE__TIMETABLE:
			return getTimetable();
		case IndTskPackage.COURSE_INSTANCE__ROLES:
			return getRoles();
		case IndTskPackage.COURSE_INSTANCE__YEAR:
			return getYear();
		case IndTskPackage.COURSE_INSTANCE__SEMESTER_TYPE:
			return getSemesterType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IndTskPackage.COURSE_INSTANCE__ID:
			setId((Integer) newValue);
			return;
		case IndTskPackage.COURSE_INSTANCE__CREDITS:
			setCredits((Double) newValue);
			return;
		case IndTskPackage.COURSE_INSTANCE__MIN_LAB_HOURS:
			setMinLabHours((Integer) newValue);
			return;
		case IndTskPackage.COURSE_INSTANCE__MIN_LEC_HOURS:
			setMinLecHours((Integer) newValue);
			return;
		case IndTskPackage.COURSE_INSTANCE__COURSE:
			setCourse((Course) newValue);
			return;
		case IndTskPackage.COURSE_INSTANCE__EVALUATIONFORM:
			setEvaluationform((EvaluationForm) newValue);
			return;
		case IndTskPackage.COURSE_INSTANCE__TIMETABLE:
			setTimetable((Timetable) newValue);
			return;
		case IndTskPackage.COURSE_INSTANCE__ROLES:
			getRoles().clear();
			getRoles().addAll((Collection<? extends Roles>) newValue);
			return;
		case IndTskPackage.COURSE_INSTANCE__YEAR:
			setYear((Integer) newValue);
			return;
		case IndTskPackage.COURSE_INSTANCE__SEMESTER_TYPE:
			setSemesterType((SemesterType) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IndTskPackage.COURSE_INSTANCE__ID:
			setId(ID_EDEFAULT);
			return;
		case IndTskPackage.COURSE_INSTANCE__CREDITS:
			setCredits(CREDITS_EDEFAULT);
			return;
		case IndTskPackage.COURSE_INSTANCE__MIN_LAB_HOURS:
			setMinLabHours(MIN_LAB_HOURS_EDEFAULT);
			return;
		case IndTskPackage.COURSE_INSTANCE__MIN_LEC_HOURS:
			setMinLecHours(MIN_LEC_HOURS_EDEFAULT);
			return;
		case IndTskPackage.COURSE_INSTANCE__COURSE:
			setCourse((Course) null);
			return;
		case IndTskPackage.COURSE_INSTANCE__EVALUATIONFORM:
			setEvaluationform((EvaluationForm) null);
			return;
		case IndTskPackage.COURSE_INSTANCE__TIMETABLE:
			setTimetable((Timetable) null);
			return;
		case IndTskPackage.COURSE_INSTANCE__ROLES:
			getRoles().clear();
			return;
		case IndTskPackage.COURSE_INSTANCE__YEAR:
			setYear(YEAR_EDEFAULT);
			return;
		case IndTskPackage.COURSE_INSTANCE__SEMESTER_TYPE:
			setSemesterType(SEMESTER_TYPE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IndTskPackage.COURSE_INSTANCE__ID:
			return id != ID_EDEFAULT;
		case IndTskPackage.COURSE_INSTANCE__CREDITS:
			return credits != CREDITS_EDEFAULT;
		case IndTskPackage.COURSE_INSTANCE__MIN_LAB_HOURS:
			return minLabHours != MIN_LAB_HOURS_EDEFAULT;
		case IndTskPackage.COURSE_INSTANCE__MIN_LEC_HOURS:
			return minLecHours != MIN_LEC_HOURS_EDEFAULT;
		case IndTskPackage.COURSE_INSTANCE__COURSE:
			return getCourse() != null;
		case IndTskPackage.COURSE_INSTANCE__EVALUATIONFORM:
			return evaluationform != null;
		case IndTskPackage.COURSE_INSTANCE__TIMETABLE:
			return timetable != null;
		case IndTskPackage.COURSE_INSTANCE__ROLES:
			return roles != null && !roles.isEmpty();
		case IndTskPackage.COURSE_INSTANCE__YEAR:
			return year != YEAR_EDEFAULT;
		case IndTskPackage.COURSE_INSTANCE__SEMESTER_TYPE:
			return semesterType != SEMESTER_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", credits: ");
		result.append(credits);
		result.append(", minLabHours: ");
		result.append(minLabHours);
		result.append(", minLecHours: ");
		result.append(minLecHours);
		result.append(", year: ");
		result.append(year);
		result.append(", semesterType: ");
		result.append(semesterType);
		result.append(')');
		return result.toString();
	}

} //CourseInstanceImpl

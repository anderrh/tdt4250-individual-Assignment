/**
 */
package tdt4250.indTsk.impl;

import org.eclipse.emf.ecore.EClass;

import tdt4250.indTsk.IndTskPackage;
import tdt4250.indTsk.Project;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ProjectImpl extends ActivityImpl implements Project {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IndTskPackage.Literals.PROJECT;
	}

} //ProjectImpl

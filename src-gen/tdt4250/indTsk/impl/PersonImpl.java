/**
 */
package tdt4250.indTsk.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.indTsk.Department;
import tdt4250.indTsk.IndTskPackage;
import tdt4250.indTsk.Person;
import tdt4250.indTsk.Roles;
import tdt4250.indTsk.StudyProgram;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.impl.PersonImpl#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.PersonImpl#getRoles <em>Roles</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.PersonImpl#getEmployedAt <em>Employed At</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.PersonImpl#getRegisteredTo <em>Registered To</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.PersonImpl#getId <em>Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PersonImpl extends MinimalEObjectImpl.Container implements Person {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRoles() <em>Roles</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoles()
	 * @generated
	 * @ordered
	 */
	protected EList<Roles> roles;

	/**
	 * The cached value of the '{@link #getEmployedAt() <em>Employed At</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmployedAt()
	 * @generated
	 * @ordered
	 */
	protected EList<Department> employedAt;

	/**
	 * The cached value of the '{@link #getRegisteredTo() <em>Registered To</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredTo()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> registeredTo;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IndTskPackage.Literals.PERSON;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.PERSON__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Roles> getRoles() {
		if (roles == null) {
			roles = new EObjectWithInverseResolvingEList.ManyInverse<Roles>(Roles.class, this,
					IndTskPackage.PERSON__ROLES, IndTskPackage.ROLES__PERSON);
		}
		return roles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Department> getEmployedAt() {
		if (employedAt == null) {
			employedAt = new EObjectWithInverseResolvingEList.ManyInverse<Department>(Department.class, this,
					IndTskPackage.PERSON__EMPLOYED_AT, IndTskPackage.DEPARTMENT__EMPLOYEES);
		}
		return employedAt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getRegisteredTo() {
		if (registeredTo == null) {
			registeredTo = new EObjectWithInverseResolvingEList.ManyInverse<StudyProgram>(StudyProgram.class, this,
					IndTskPackage.PERSON__REGISTERED_TO, IndTskPackage.STUDY_PROGRAM__STUDENTS);
		}
		return registeredTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.PERSON__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.PERSON__ROLES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getRoles()).basicAdd(otherEnd, msgs);
		case IndTskPackage.PERSON__EMPLOYED_AT:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getEmployedAt()).basicAdd(otherEnd, msgs);
		case IndTskPackage.PERSON__REGISTERED_TO:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getRegisteredTo()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.PERSON__ROLES:
			return ((InternalEList<?>) getRoles()).basicRemove(otherEnd, msgs);
		case IndTskPackage.PERSON__EMPLOYED_AT:
			return ((InternalEList<?>) getEmployedAt()).basicRemove(otherEnd, msgs);
		case IndTskPackage.PERSON__REGISTERED_TO:
			return ((InternalEList<?>) getRegisteredTo()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IndTskPackage.PERSON__NAME:
			return getName();
		case IndTskPackage.PERSON__ROLES:
			return getRoles();
		case IndTskPackage.PERSON__EMPLOYED_AT:
			return getEmployedAt();
		case IndTskPackage.PERSON__REGISTERED_TO:
			return getRegisteredTo();
		case IndTskPackage.PERSON__ID:
			return getId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IndTskPackage.PERSON__NAME:
			setName((String) newValue);
			return;
		case IndTskPackage.PERSON__ROLES:
			getRoles().clear();
			getRoles().addAll((Collection<? extends Roles>) newValue);
			return;
		case IndTskPackage.PERSON__EMPLOYED_AT:
			getEmployedAt().clear();
			getEmployedAt().addAll((Collection<? extends Department>) newValue);
			return;
		case IndTskPackage.PERSON__REGISTERED_TO:
			getRegisteredTo().clear();
			getRegisteredTo().addAll((Collection<? extends StudyProgram>) newValue);
			return;
		case IndTskPackage.PERSON__ID:
			setId((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IndTskPackage.PERSON__NAME:
			setName(NAME_EDEFAULT);
			return;
		case IndTskPackage.PERSON__ROLES:
			getRoles().clear();
			return;
		case IndTskPackage.PERSON__EMPLOYED_AT:
			getEmployedAt().clear();
			return;
		case IndTskPackage.PERSON__REGISTERED_TO:
			getRegisteredTo().clear();
			return;
		case IndTskPackage.PERSON__ID:
			setId(ID_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IndTskPackage.PERSON__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case IndTskPackage.PERSON__ROLES:
			return roles != null && !roles.isEmpty();
		case IndTskPackage.PERSON__EMPLOYED_AT:
			return employedAt != null && !employedAt.isEmpty();
		case IndTskPackage.PERSON__REGISTERED_TO:
			return registeredTo != null && !registeredTo.isEmpty();
		case IndTskPackage.PERSON__ID:
			return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //PersonImpl

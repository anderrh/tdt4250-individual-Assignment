/**
 */
package tdt4250.indTsk.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.indTsk.Activity;
import tdt4250.indTsk.CourseInstance;
import tdt4250.indTsk.EvaluationForm;
import tdt4250.indTsk.IndTskPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Evaluation Form</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.impl.EvaluationFormImpl#getHasActivity <em>Has Activity</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.EvaluationFormImpl#getCourseinstance <em>Courseinstance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EvaluationFormImpl extends MinimalEObjectImpl.Container implements EvaluationForm {
	/**
	 * The cached value of the '{@link #getHasActivity() <em>Has Activity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasActivity()
	 * @generated
	 * @ordered
	 */
	protected EList<Activity> hasActivity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EvaluationFormImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IndTskPackage.Literals.EVALUATION_FORM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Activity> getHasActivity() {
		if (hasActivity == null) {
			hasActivity = new EObjectContainmentEList<Activity>(Activity.class, this,
					IndTskPackage.EVALUATION_FORM__HAS_ACTIVITY);
		}
		return hasActivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseInstance getCourseinstance() {
		if (eContainerFeatureID() != IndTskPackage.EVALUATION_FORM__COURSEINSTANCE)
			return null;
		return (CourseInstance) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourseinstance(CourseInstance newCourseinstance, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCourseinstance, IndTskPackage.EVALUATION_FORM__COURSEINSTANCE,
				msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseinstance(CourseInstance newCourseinstance) {
		if (newCourseinstance != eInternalContainer()
				|| (eContainerFeatureID() != IndTskPackage.EVALUATION_FORM__COURSEINSTANCE
						&& newCourseinstance != null)) {
			if (EcoreUtil.isAncestor(this, newCourseinstance))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourseinstance != null)
				msgs = ((InternalEObject) newCourseinstance).eInverseAdd(this,
						IndTskPackage.COURSE_INSTANCE__EVALUATIONFORM, CourseInstance.class, msgs);
			msgs = basicSetCourseinstance(newCourseinstance, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.EVALUATION_FORM__COURSEINSTANCE,
					newCourseinstance, newCourseinstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.EVALUATION_FORM__COURSEINSTANCE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCourseinstance((CourseInstance) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.EVALUATION_FORM__HAS_ACTIVITY:
			return ((InternalEList<?>) getHasActivity()).basicRemove(otherEnd, msgs);
		case IndTskPackage.EVALUATION_FORM__COURSEINSTANCE:
			return basicSetCourseinstance(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case IndTskPackage.EVALUATION_FORM__COURSEINSTANCE:
			return eInternalContainer().eInverseRemove(this, IndTskPackage.COURSE_INSTANCE__EVALUATIONFORM,
					CourseInstance.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IndTskPackage.EVALUATION_FORM__HAS_ACTIVITY:
			return getHasActivity();
		case IndTskPackage.EVALUATION_FORM__COURSEINSTANCE:
			return getCourseinstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IndTskPackage.EVALUATION_FORM__HAS_ACTIVITY:
			getHasActivity().clear();
			getHasActivity().addAll((Collection<? extends Activity>) newValue);
			return;
		case IndTskPackage.EVALUATION_FORM__COURSEINSTANCE:
			setCourseinstance((CourseInstance) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IndTskPackage.EVALUATION_FORM__HAS_ACTIVITY:
			getHasActivity().clear();
			return;
		case IndTskPackage.EVALUATION_FORM__COURSEINSTANCE:
			setCourseinstance((CourseInstance) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IndTskPackage.EVALUATION_FORM__HAS_ACTIVITY:
			return hasActivity != null && !hasActivity.isEmpty();
		case IndTskPackage.EVALUATION_FORM__COURSEINSTANCE:
			return getCourseinstance() != null;
		}
		return super.eIsSet(featureID);
	}

} //EvaluationFormImpl

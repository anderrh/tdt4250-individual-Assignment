/**
 */
package tdt4250.indTsk.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.indTsk.Course;
import tdt4250.indTsk.CourseInstance;
import tdt4250.indTsk.Department;
import tdt4250.indTsk.IndTskPackage;
import tdt4250.indTsk.StudyProgram;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.impl.CourseImpl#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseImpl#getCourseCode <em>Course Code</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseImpl#getContent <em>Content</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseImpl#getCourseinstance <em>Courseinstance</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseImpl#getPartOf <em>Part Of</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseImpl#getResponsibleDepartment <em>Responsible Department</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.CourseImpl#getRequiresCourse <em>Requires Course</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends MinimalEObjectImpl.Container implements Course {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getCourseCode() <em>Course Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseCode()
	 * @generated
	 * @ordered
	 */
	protected static final String COURSE_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCourseCode() <em>Course Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseCode()
	 * @generated
	 * @ordered
	 */
	protected String courseCode = COURSE_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected String content = CONTENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCourseinstance() <em>Courseinstance</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseinstance()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseInstance> courseinstance;

	/**
	 * The cached value of the '{@link #getPartOf() <em>Part Of</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartOf()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> partOf;

	/**
	 * The cached value of the '{@link #getResponsibleDepartment() <em>Responsible Department</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibleDepartment()
	 * @generated
	 * @ordered
	 */
	protected Department responsibleDepartment;

	/**
	 * The cached value of the '{@link #getRequiresCourse() <em>Requires Course</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiresCourse()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> requiresCourse;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IndTskPackage.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.COURSE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCourseCode() {
		return courseCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseCode(String newCourseCode) {
		String oldCourseCode = courseCode;
		courseCode = newCourseCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.COURSE__COURSE_CODE, oldCourseCode,
					courseCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(String newContent) {
		String oldContent = content;
		content = newContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.COURSE__CONTENT, oldContent, content));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseInstance> getCourseinstance() {
		if (courseinstance == null) {
			courseinstance = new EObjectContainmentWithInverseEList<CourseInstance>(CourseInstance.class, this,
					IndTskPackage.COURSE__COURSEINSTANCE, IndTskPackage.COURSE_INSTANCE__COURSE);
		}
		return courseinstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getPartOf() {
		if (partOf == null) {
			partOf = new EObjectWithInverseResolvingEList.ManyInverse<StudyProgram>(StudyProgram.class, this,
					IndTskPackage.COURSE__PART_OF, IndTskPackage.STUDY_PROGRAM__CONSISTS_OF);
		}
		return partOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department getResponsibleDepartment() {
		if (responsibleDepartment != null && responsibleDepartment.eIsProxy()) {
			InternalEObject oldResponsibleDepartment = (InternalEObject) responsibleDepartment;
			responsibleDepartment = (Department) eResolveProxy(oldResponsibleDepartment);
			if (responsibleDepartment != oldResponsibleDepartment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							IndTskPackage.COURSE__RESPONSIBLE_DEPARTMENT, oldResponsibleDepartment,
							responsibleDepartment));
			}
		}
		return responsibleDepartment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department basicGetResponsibleDepartment() {
		return responsibleDepartment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResponsibleDepartment(Department newResponsibleDepartment,
			NotificationChain msgs) {
		Department oldResponsibleDepartment = responsibleDepartment;
		responsibleDepartment = newResponsibleDepartment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					IndTskPackage.COURSE__RESPONSIBLE_DEPARTMENT, oldResponsibleDepartment, newResponsibleDepartment);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponsibleDepartment(Department newResponsibleDepartment) {
		if (newResponsibleDepartment != responsibleDepartment) {
			NotificationChain msgs = null;
			if (responsibleDepartment != null)
				msgs = ((InternalEObject) responsibleDepartment).eInverseRemove(this,
						IndTskPackage.DEPARTMENT__RESPONSIBLE_FOR, Department.class, msgs);
			if (newResponsibleDepartment != null)
				msgs = ((InternalEObject) newResponsibleDepartment).eInverseAdd(this,
						IndTskPackage.DEPARTMENT__RESPONSIBLE_FOR, Department.class, msgs);
			msgs = basicSetResponsibleDepartment(newResponsibleDepartment, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.COURSE__RESPONSIBLE_DEPARTMENT,
					newResponsibleDepartment, newResponsibleDepartment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getRequiresCourse() {
		if (requiresCourse == null) {
			requiresCourse = new EObjectResolvingEList<Course>(Course.class, this,
					IndTskPackage.COURSE__REQUIRES_COURSE);
		}
		return requiresCourse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.COURSE__COURSEINSTANCE:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getCourseinstance()).basicAdd(otherEnd, msgs);
		case IndTskPackage.COURSE__PART_OF:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getPartOf()).basicAdd(otherEnd, msgs);
		case IndTskPackage.COURSE__RESPONSIBLE_DEPARTMENT:
			if (responsibleDepartment != null)
				msgs = ((InternalEObject) responsibleDepartment).eInverseRemove(this,
						IndTskPackage.DEPARTMENT__RESPONSIBLE_FOR, Department.class, msgs);
			return basicSetResponsibleDepartment((Department) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.COURSE__COURSEINSTANCE:
			return ((InternalEList<?>) getCourseinstance()).basicRemove(otherEnd, msgs);
		case IndTskPackage.COURSE__PART_OF:
			return ((InternalEList<?>) getPartOf()).basicRemove(otherEnd, msgs);
		case IndTskPackage.COURSE__RESPONSIBLE_DEPARTMENT:
			return basicSetResponsibleDepartment(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IndTskPackage.COURSE__NAME:
			return getName();
		case IndTskPackage.COURSE__COURSE_CODE:
			return getCourseCode();
		case IndTskPackage.COURSE__CONTENT:
			return getContent();
		case IndTskPackage.COURSE__COURSEINSTANCE:
			return getCourseinstance();
		case IndTskPackage.COURSE__PART_OF:
			return getPartOf();
		case IndTskPackage.COURSE__RESPONSIBLE_DEPARTMENT:
			if (resolve)
				return getResponsibleDepartment();
			return basicGetResponsibleDepartment();
		case IndTskPackage.COURSE__REQUIRES_COURSE:
			return getRequiresCourse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IndTskPackage.COURSE__NAME:
			setName((String) newValue);
			return;
		case IndTskPackage.COURSE__COURSE_CODE:
			setCourseCode((String) newValue);
			return;
		case IndTskPackage.COURSE__CONTENT:
			setContent((String) newValue);
			return;
		case IndTskPackage.COURSE__COURSEINSTANCE:
			getCourseinstance().clear();
			getCourseinstance().addAll((Collection<? extends CourseInstance>) newValue);
			return;
		case IndTskPackage.COURSE__PART_OF:
			getPartOf().clear();
			getPartOf().addAll((Collection<? extends StudyProgram>) newValue);
			return;
		case IndTskPackage.COURSE__RESPONSIBLE_DEPARTMENT:
			setResponsibleDepartment((Department) newValue);
			return;
		case IndTskPackage.COURSE__REQUIRES_COURSE:
			getRequiresCourse().clear();
			getRequiresCourse().addAll((Collection<? extends Course>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IndTskPackage.COURSE__NAME:
			setName(NAME_EDEFAULT);
			return;
		case IndTskPackage.COURSE__COURSE_CODE:
			setCourseCode(COURSE_CODE_EDEFAULT);
			return;
		case IndTskPackage.COURSE__CONTENT:
			setContent(CONTENT_EDEFAULT);
			return;
		case IndTskPackage.COURSE__COURSEINSTANCE:
			getCourseinstance().clear();
			return;
		case IndTskPackage.COURSE__PART_OF:
			getPartOf().clear();
			return;
		case IndTskPackage.COURSE__RESPONSIBLE_DEPARTMENT:
			setResponsibleDepartment((Department) null);
			return;
		case IndTskPackage.COURSE__REQUIRES_COURSE:
			getRequiresCourse().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IndTskPackage.COURSE__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case IndTskPackage.COURSE__COURSE_CODE:
			return COURSE_CODE_EDEFAULT == null ? courseCode != null : !COURSE_CODE_EDEFAULT.equals(courseCode);
		case IndTskPackage.COURSE__CONTENT:
			return CONTENT_EDEFAULT == null ? content != null : !CONTENT_EDEFAULT.equals(content);
		case IndTskPackage.COURSE__COURSEINSTANCE:
			return courseinstance != null && !courseinstance.isEmpty();
		case IndTskPackage.COURSE__PART_OF:
			return partOf != null && !partOf.isEmpty();
		case IndTskPackage.COURSE__RESPONSIBLE_DEPARTMENT:
			return responsibleDepartment != null;
		case IndTskPackage.COURSE__REQUIRES_COURSE:
			return requiresCourse != null && !requiresCourse.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", courseCode: ");
		result.append(courseCode);
		result.append(", content: ");
		result.append(content);
		result.append(')');
		return result.toString();
	}

} //CourseImpl

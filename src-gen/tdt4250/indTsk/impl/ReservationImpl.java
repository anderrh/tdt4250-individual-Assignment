/**
 */
package tdt4250.indTsk.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import tdt4250.indTsk.Day;
import tdt4250.indTsk.IndTskPackage;
import tdt4250.indTsk.Reservation;
import tdt4250.indTsk.ReservationType;
import tdt4250.indTsk.Room;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reservation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.impl.ReservationImpl#getDay <em>Day</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.ReservationImpl#getReservationType <em>Reservation Type</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.ReservationImpl#getFromHour <em>From Hour</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.ReservationImpl#getToHour <em>To Hour</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.ReservationImpl#getReservedRoom <em>Reserved Room</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReservationImpl extends MinimalEObjectImpl.Container implements Reservation {
	/**
	 * The default value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected static final Day DAY_EDEFAULT = Day.MONDAY;

	/**
	 * The cached value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected Day day = DAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getReservationType() <em>Reservation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservationType()
	 * @generated
	 * @ordered
	 */
	protected static final ReservationType RESERVATION_TYPE_EDEFAULT = ReservationType.LECTURE;

	/**
	 * The cached value of the '{@link #getReservationType() <em>Reservation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservationType()
	 * @generated
	 * @ordered
	 */
	protected ReservationType reservationType = RESERVATION_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFromHour() <em>From Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromHour()
	 * @generated
	 * @ordered
	 */
	protected static final int FROM_HOUR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFromHour() <em>From Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromHour()
	 * @generated
	 * @ordered
	 */
	protected int fromHour = FROM_HOUR_EDEFAULT;

	/**
	 * The default value of the '{@link #getToHour() <em>To Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToHour()
	 * @generated
	 * @ordered
	 */
	protected static final int TO_HOUR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getToHour() <em>To Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToHour()
	 * @generated
	 * @ordered
	 */
	protected int toHour = TO_HOUR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReservedRoom() <em>Reserved Room</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservedRoom()
	 * @generated
	 * @ordered
	 */
	protected Room reservedRoom;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReservationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IndTskPackage.Literals.RESERVATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Day getDay() {
		return day;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDay(Day newDay) {
		Day oldDay = day;
		day = newDay == null ? DAY_EDEFAULT : newDay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.RESERVATION__DAY, oldDay, day));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReservationType getReservationType() {
		return reservationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReservationType(ReservationType newReservationType) {
		ReservationType oldReservationType = reservationType;
		reservationType = newReservationType == null ? RESERVATION_TYPE_EDEFAULT : newReservationType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.RESERVATION__RESERVATION_TYPE,
					oldReservationType, reservationType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFromHour() {
		return fromHour;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromHour(int newFromHour) {
		int oldFromHour = fromHour;
		fromHour = newFromHour;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.RESERVATION__FROM_HOUR, oldFromHour,
					fromHour));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getToHour() {
		return toHour;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToHour(int newToHour) {
		int oldToHour = toHour;
		toHour = newToHour;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.RESERVATION__TO_HOUR, oldToHour,
					toHour));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room getReservedRoom() {
		if (reservedRoom != null && reservedRoom.eIsProxy()) {
			InternalEObject oldReservedRoom = (InternalEObject) reservedRoom;
			reservedRoom = (Room) eResolveProxy(oldReservedRoom);
			if (reservedRoom != oldReservedRoom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IndTskPackage.RESERVATION__RESERVED_ROOM,
							oldReservedRoom, reservedRoom));
			}
		}
		return reservedRoom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room basicGetReservedRoom() {
		return reservedRoom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReservedRoom(Room newReservedRoom, NotificationChain msgs) {
		Room oldReservedRoom = reservedRoom;
		reservedRoom = newReservedRoom;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					IndTskPackage.RESERVATION__RESERVED_ROOM, oldReservedRoom, newReservedRoom);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReservedRoom(Room newReservedRoom) {
		if (newReservedRoom != reservedRoom) {
			NotificationChain msgs = null;
			if (reservedRoom != null)
				msgs = ((InternalEObject) reservedRoom).eInverseRemove(this, IndTskPackage.ROOM__RESERVED_TO,
						Room.class, msgs);
			if (newReservedRoom != null)
				msgs = ((InternalEObject) newReservedRoom).eInverseAdd(this, IndTskPackage.ROOM__RESERVED_TO,
						Room.class, msgs);
			msgs = basicSetReservedRoom(newReservedRoom, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.RESERVATION__RESERVED_ROOM,
					newReservedRoom, newReservedRoom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.RESERVATION__RESERVED_ROOM:
			if (reservedRoom != null)
				msgs = ((InternalEObject) reservedRoom).eInverseRemove(this, IndTskPackage.ROOM__RESERVED_TO,
						Room.class, msgs);
			return basicSetReservedRoom((Room) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.RESERVATION__RESERVED_ROOM:
			return basicSetReservedRoom(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IndTskPackage.RESERVATION__DAY:
			return getDay();
		case IndTskPackage.RESERVATION__RESERVATION_TYPE:
			return getReservationType();
		case IndTskPackage.RESERVATION__FROM_HOUR:
			return getFromHour();
		case IndTskPackage.RESERVATION__TO_HOUR:
			return getToHour();
		case IndTskPackage.RESERVATION__RESERVED_ROOM:
			if (resolve)
				return getReservedRoom();
			return basicGetReservedRoom();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IndTskPackage.RESERVATION__DAY:
			setDay((Day) newValue);
			return;
		case IndTskPackage.RESERVATION__RESERVATION_TYPE:
			setReservationType((ReservationType) newValue);
			return;
		case IndTskPackage.RESERVATION__FROM_HOUR:
			setFromHour((Integer) newValue);
			return;
		case IndTskPackage.RESERVATION__TO_HOUR:
			setToHour((Integer) newValue);
			return;
		case IndTskPackage.RESERVATION__RESERVED_ROOM:
			setReservedRoom((Room) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IndTskPackage.RESERVATION__DAY:
			setDay(DAY_EDEFAULT);
			return;
		case IndTskPackage.RESERVATION__RESERVATION_TYPE:
			setReservationType(RESERVATION_TYPE_EDEFAULT);
			return;
		case IndTskPackage.RESERVATION__FROM_HOUR:
			setFromHour(FROM_HOUR_EDEFAULT);
			return;
		case IndTskPackage.RESERVATION__TO_HOUR:
			setToHour(TO_HOUR_EDEFAULT);
			return;
		case IndTskPackage.RESERVATION__RESERVED_ROOM:
			setReservedRoom((Room) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IndTskPackage.RESERVATION__DAY:
			return day != DAY_EDEFAULT;
		case IndTskPackage.RESERVATION__RESERVATION_TYPE:
			return reservationType != RESERVATION_TYPE_EDEFAULT;
		case IndTskPackage.RESERVATION__FROM_HOUR:
			return fromHour != FROM_HOUR_EDEFAULT;
		case IndTskPackage.RESERVATION__TO_HOUR:
			return toHour != TO_HOUR_EDEFAULT;
		case IndTskPackage.RESERVATION__RESERVED_ROOM:
			return reservedRoom != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (day: ");
		result.append(day);
		result.append(", reservationType: ");
		result.append(reservationType);
		result.append(", fromHour: ");
		result.append(fromHour);
		result.append(", toHour: ");
		result.append(toHour);
		result.append(')');
		return result.toString();
	}

} //ReservationImpl

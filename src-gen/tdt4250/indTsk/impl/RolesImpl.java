/**
 */
package tdt4250.indTsk.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.indTsk.CourseInstance;
import tdt4250.indTsk.IndTskPackage;
import tdt4250.indTsk.Person;
import tdt4250.indTsk.RoleType;
import tdt4250.indTsk.Roles;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Roles</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.impl.RolesImpl#getPerson <em>Person</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.RolesImpl#getCourseinstance <em>Courseinstance</em>}</li>
 *   <li>{@link tdt4250.indTsk.impl.RolesImpl#getRoleType <em>Role Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RolesImpl extends MinimalEObjectImpl.Container implements Roles {
	/**
	 * The cached value of the '{@link #getPerson() <em>Person</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPerson()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> person;

	/**
	 * The default value of the '{@link #getRoleType() <em>Role Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleType()
	 * @generated
	 * @ordered
	 */
	protected static final RoleType ROLE_TYPE_EDEFAULT = RoleType.STUDENT;

	/**
	 * The cached value of the '{@link #getRoleType() <em>Role Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleType()
	 * @generated
	 * @ordered
	 */
	protected RoleType roleType = ROLE_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RolesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IndTskPackage.Literals.ROLES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Person> getPerson() {
		if (person == null) {
			person = new EObjectWithInverseResolvingEList.ManyInverse<Person>(Person.class, this,
					IndTskPackage.ROLES__PERSON, IndTskPackage.PERSON__ROLES);
		}
		return person;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseInstance getCourseinstance() {
		if (eContainerFeatureID() != IndTskPackage.ROLES__COURSEINSTANCE)
			return null;
		return (CourseInstance) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourseinstance(CourseInstance newCourseinstance, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCourseinstance, IndTskPackage.ROLES__COURSEINSTANCE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseinstance(CourseInstance newCourseinstance) {
		if (newCourseinstance != eInternalContainer()
				|| (eContainerFeatureID() != IndTskPackage.ROLES__COURSEINSTANCE && newCourseinstance != null)) {
			if (EcoreUtil.isAncestor(this, newCourseinstance))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourseinstance != null)
				msgs = ((InternalEObject) newCourseinstance).eInverseAdd(this, IndTskPackage.COURSE_INSTANCE__ROLES,
						CourseInstance.class, msgs);
			msgs = basicSetCourseinstance(newCourseinstance, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.ROLES__COURSEINSTANCE,
					newCourseinstance, newCourseinstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleType getRoleType() {
		return roleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoleType(RoleType newRoleType) {
		RoleType oldRoleType = roleType;
		roleType = newRoleType == null ? ROLE_TYPE_EDEFAULT : newRoleType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IndTskPackage.ROLES__ROLE_TYPE, oldRoleType,
					roleType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.ROLES__PERSON:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getPerson()).basicAdd(otherEnd, msgs);
		case IndTskPackage.ROLES__COURSEINSTANCE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCourseinstance((CourseInstance) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case IndTskPackage.ROLES__PERSON:
			return ((InternalEList<?>) getPerson()).basicRemove(otherEnd, msgs);
		case IndTskPackage.ROLES__COURSEINSTANCE:
			return basicSetCourseinstance(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case IndTskPackage.ROLES__COURSEINSTANCE:
			return eInternalContainer().eInverseRemove(this, IndTskPackage.COURSE_INSTANCE__ROLES, CourseInstance.class,
					msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IndTskPackage.ROLES__PERSON:
			return getPerson();
		case IndTskPackage.ROLES__COURSEINSTANCE:
			return getCourseinstance();
		case IndTskPackage.ROLES__ROLE_TYPE:
			return getRoleType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IndTskPackage.ROLES__PERSON:
			getPerson().clear();
			getPerson().addAll((Collection<? extends Person>) newValue);
			return;
		case IndTskPackage.ROLES__COURSEINSTANCE:
			setCourseinstance((CourseInstance) newValue);
			return;
		case IndTskPackage.ROLES__ROLE_TYPE:
			setRoleType((RoleType) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IndTskPackage.ROLES__PERSON:
			getPerson().clear();
			return;
		case IndTskPackage.ROLES__COURSEINSTANCE:
			setCourseinstance((CourseInstance) null);
			return;
		case IndTskPackage.ROLES__ROLE_TYPE:
			setRoleType(ROLE_TYPE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IndTskPackage.ROLES__PERSON:
			return person != null && !person.isEmpty();
		case IndTskPackage.ROLES__COURSEINSTANCE:
			return getCourseinstance() != null;
		case IndTskPackage.ROLES__ROLE_TYPE:
			return roleType != ROLE_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (roleType: ");
		result.append(roleType);
		result.append(')');
		return result.toString();
	}

} //RolesImpl

/**
 */
package tdt4250.indTsk;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.Room#getRoomID <em>Room ID</em>}</li>
 *   <li>{@link tdt4250.indTsk.Room#getReservedTo <em>Reserved To</em>}</li>
 * </ul>
 *
 * @see tdt4250.indTsk.IndTskPackage#getRoom()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='noOverlappingRoomReservations'"
 * @generated
 */
public interface Room extends EObject {
	/**
	 * Returns the value of the '<em><b>Room ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room ID</em>' attribute.
	 * @see #setRoomID(String)
	 * @see tdt4250.indTsk.IndTskPackage#getRoom_RoomID()
	 * @model
	 * @generated
	 */
	String getRoomID();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Room#getRoomID <em>Room ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room ID</em>' attribute.
	 * @see #getRoomID()
	 * @generated
	 */
	void setRoomID(String value);

	/**
	 * Returns the value of the '<em><b>Reserved To</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.indTsk.Reservation}.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.Reservation#getReservedRoom <em>Reserved Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reserved To</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reserved To</em>' reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getRoom_ReservedTo()
	 * @see tdt4250.indTsk.Reservation#getReservedRoom
	 * @model opposite="reservedRoom"
	 * @generated
	 */
	EList<Reservation> getReservedTo();

} // Room

/**
 */
package tdt4250.indTsk.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import tdt4250.indTsk.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see tdt4250.indTsk.IndTskPackage
 * @generated
 */
public class IndTskValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final IndTskValidator INSTANCE = new IndTskValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "tdt4250.indTsk";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndTskValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
		return IndTskPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		switch (classifierID) {
		case IndTskPackage.COURSE:
			return validateCourse((Course) value, diagnostics, context);
		case IndTskPackage.COURSE_INSTANCE:
			return validateCourseInstance((CourseInstance) value, diagnostics, context);
		case IndTskPackage.PERSON:
			return validatePerson((Person) value, diagnostics, context);
		case IndTskPackage.RESERVATION:
			return validateReservation((Reservation) value, diagnostics, context);
		case IndTskPackage.ASSIGNMENT:
			return validateAssignment((Assignment) value, diagnostics, context);
		case IndTskPackage.EXAM:
			return validateExam((Exam) value, diagnostics, context);
		case IndTskPackage.EVALUATION_FORM:
			return validateEvaluationForm((EvaluationForm) value, diagnostics, context);
		case IndTskPackage.TIMETABLE:
			return validateTimetable((Timetable) value, diagnostics, context);
		case IndTskPackage.DEPARTMENT:
			return validateDepartment((Department) value, diagnostics, context);
		case IndTskPackage.ACTIVITY:
			return validateActivity((Activity) value, diagnostics, context);
		case IndTskPackage.STUDY_PROGRAM:
			return validateStudyProgram((StudyProgram) value, diagnostics, context);
		case IndTskPackage.PROJECT:
			return validateProject((Project) value, diagnostics, context);
		case IndTskPackage.SCHOOL:
			return validateSchool((School) value, diagnostics, context);
		case IndTskPackage.ROOM:
			return validateRoom((Room) value, diagnostics, context);
		case IndTskPackage.ROLES:
			return validateRoles((Roles) value, diagnostics, context);
		case IndTskPackage.ROLE_TYPE:
			return validateRoleType((RoleType) value, diagnostics, context);
		case IndTskPackage.DAY:
			return validateDay((Day) value, diagnostics, context);
		case IndTskPackage.SEMESTER_TYPE:
			return validateSemesterType((SemesterType) value, diagnostics, context);
		case IndTskPackage.RESERVATION_TYPE:
			return validateReservationType((ReservationType) value, diagnostics, context);
		default:
			return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(course, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseInstance(CourseInstance courseInstance, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(courseInstance, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(courseInstance, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateCourseInstance_correctHourSum(courseInstance, diagnostics, context);
		return result;
	}

	/**
	 * Validates the correctHourSum constraint of '<em>Course Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateCourseInstance_correctHourSum(CourseInstance courseInstance, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if ((courseInstance.getTimetable().getTotalLabHours() < courseInstance.getMinLabHours())
				|| (courseInstance.getTimetable().getTotalLecHours() < courseInstance.getMinLecHours())) {
			if (diagnostics != null) {
				diagnostics.add(
						createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0, "_UI_GenericConstraint_diagnostic",
								new Object[] { "correctSum", getObjectLabel(courseInstance, context) },
								new Object[] { courseInstance }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePerson(Person person, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(person, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReservation(Reservation reservation, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(reservation, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(reservation, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(reservation, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(reservation, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(reservation, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(reservation, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(reservation, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(reservation, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(reservation, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateReservation_fromLowerThanTo(reservation, diagnostics, context);
		return result;
	}

	/**
	 * Validates the fromLowerThanTo constraint of '<em>Reservation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateReservation_fromLowerThanTo(Reservation reservation, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (reservation.getFromHour() < reservation.getToHour()) {
			if (diagnostics != null) {
				diagnostics.add(
						createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0, "_UI_GenericConstraint_diagnostic",
								new Object[] { "fromLowerThanTo", getObjectLabel(reservation, context) },
								new Object[] { reservation }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssignment(Assignment assignment, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(assignment, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExam(Exam exam, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(exam, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEvaluationForm(EvaluationForm evaluationForm, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(evaluationForm, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTimetable(Timetable timetable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(timetable, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDepartment(Department department, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(department, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateActivity(Activity activity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(activity, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStudyProgram(StudyProgram studyProgram, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(studyProgram, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProject(Project project, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(project, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSchool(School school, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(school, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRoom(Room room, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(room, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(room, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(room, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(room, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(room, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(room, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(room, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(room, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(room, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateRoom_noOverlappingRoomReservations(room, diagnostics, context);
		return result;
	}

	/**
	 * Validates the noOverlappingRoomReservations constraint of '<em>Room</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateRoom_noOverlappingRoomReservations(Room room, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (anyOverlap(room)) {
			if (diagnostics != null) {
				diagnostics.add(
						createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0, "_UI_GenericConstraint_diagnostic",
								new Object[] { "noOverlappingRoomReservations", getObjectLabel(room, context) },
								new Object[] { room }, context));
			}
			return false;
		}
		return true;
	}
	// Helper method
	private boolean anyOverlap(Room room) {
		List<Reservation> reservations = room.getReservedTo();
		List<int[]> slots = new ArrayList<>();
		for (Reservation reservation : reservations) {
			int resFrom = reservation.getFromHour();
			int resTo = reservation.getToHour();
			int resDay = reservation.getDay().getValue();
			
			for (int[] slot : slots) {
				if(resDay == slot[2]) {
					if((resFrom <= slot[1]) && (resTo <= slot[0])) {
						return true;
					}
				}
			}
			slots.add(new int[] {resFrom, resTo, resDay});
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRoles(Roles roles, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(roles, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRoleType(RoleType roleType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDay(Day day, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSemesterType(SemesterType semesterType, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReservationType(ReservationType reservationType, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //IndTskValidator

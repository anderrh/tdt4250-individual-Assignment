/**
 */
package tdt4250.indTsk;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.indTsk.Department#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.indTsk.Department#getResponsibleFor <em>Responsible For</em>}</li>
 *   <li>{@link tdt4250.indTsk.Department#getEmployees <em>Employees</em>}</li>
 * </ul>
 *
 * @see tdt4250.indTsk.IndTskPackage#getDepartment()
 * @model
 * @generated
 */
public interface Department extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.indTsk.IndTskPackage#getDepartment_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.indTsk.Department#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Responsible For</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.indTsk.Course}.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.Course#getResponsibleDepartment <em>Responsible Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible For</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible For</em>' reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getDepartment_ResponsibleFor()
	 * @see tdt4250.indTsk.Course#getResponsibleDepartment
	 * @model opposite="responsibleDepartment"
	 * @generated
	 */
	EList<Course> getResponsibleFor();

	/**
	 * Returns the value of the '<em><b>Employees</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.indTsk.Person}.
	 * It is bidirectional and its opposite is '{@link tdt4250.indTsk.Person#getEmployedAt <em>Employed At</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Employees</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Employees</em>' reference list.
	 * @see tdt4250.indTsk.IndTskPackage#getDepartment_Employees()
	 * @see tdt4250.indTsk.Person#getEmployedAt
	 * @model opposite="employedAt"
	 * @generated
	 */
	EList<Person> getEmployees();

} // Department

package tdt4250.indTsk.html;

import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;
import tdt4250.indTsk.Course;
import tdt4250.indTsk.CourseInstance;
import tdt4250.indTsk.School;
import tdt4250.indTsk.SemesterType;
import tdt4250.indTsk.StudyProgram;

@SuppressWarnings("all")
public class School2TextGenerator {
  private int indentCount = 0;
  
  private String getIndent() {
    String _xblockexpression = null;
    {
      String retString = "";
      ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, this.indentCount, true);
      for (final Integer i : _doubleDotLessThan) {
        String _retString = retString;
        retString = (_retString + "  ");
      }
      _xblockexpression = retString;
    }
    return _xblockexpression;
  }
  
  public String generateHtml(final School school) {
    StringBuilder _stringBuilder = new StringBuilder();
    return this.generateHtml(school, _stringBuilder).toString();
  }
  
  public CharSequence generateHtml(final School school, final StringBuilder sBuilder) {
    CharSequence _xblockexpression = null;
    {
      String _name = school.getName();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("p {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("margin-top: 5px;");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("h1, h2, h3, h4, h5, h6 {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("margin-bottom: 0px;");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("ul {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("margin: 0px;");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append(".courseContainer {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("border: 1px solid #ccc!important;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("border-radius: 4px;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("margin: 10px;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("padding-left: 10px;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("display: flex;");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append(".courseContainer::after {");
      _builder.newLine();
      _builder.append("    ");
      _builder.append("content: \"\";");
      _builder.newLine();
      _builder.append("    ");
      _builder.append("clear: both;");
      _builder.newLine();
      _builder.append("    ");
      _builder.append("display: table;");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append(".courseMainInfo {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("float: left;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("width: 74%;");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append(".courseSideBar {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("border: 1px solid #ccc!important;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("border-radius: 4px;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("float: right;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("width: 25%;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("padding: 5px;");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append(".courseSubtitle {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("margin-top: 0px;");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      this.applyPreHtml(_name, sBuilder, _builder.toString());
      String _name_1 = school.getName();
      String _plus = ("Velkommen til " + _name_1);
      this.genereateHeader(_plus, 1, sBuilder);
      final Consumer<Course> _function = (Course it) -> {
        this.generateCourseElement(it, sBuilder);
      };
      school.getCourses().forEach(_function);
      _xblockexpression = this.applyPostHtml(sBuilder);
    }
    return _xblockexpression;
  }
  
  public StringBuilder generateCourseElement(final Course course, final StringBuilder sBuilder) {
    StringBuilder _xblockexpression = null;
    {
      StringConcatenation _builder = new StringConcatenation();
      String _indent = this.getIndent();
      _builder.append(_indent);
      _builder.append("<div class=courseContainer>");
      _builder.newLineIfNotEmpty();
      String _indent_1 = this.getIndent();
      _builder.append(_indent_1);
      _builder.append("  <div class=courseMainInfo>");
      _builder.newLineIfNotEmpty();
      String _indent_2 = this.getIndent();
      _builder.append(_indent_2);
      _builder.append("    <h3>");
      String _courseCode = course.getCourseCode();
      _builder.append(_courseCode);
      _builder.append(" - ");
      String _name = course.getName();
      _builder.append(_name);
      _builder.append("</h3>");
      _builder.newLineIfNotEmpty();
      String _indent_3 = this.getIndent();
      _builder.append(_indent_3);
      _builder.append("    <h5 class=\"courseSubtitle\">");
      String _name_1 = course.getResponsibleDepartment().getName();
      _builder.append(_name_1);
      _builder.append("</h5>");
      _builder.newLineIfNotEmpty();
      String _indent_4 = this.getIndent();
      _builder.append(_indent_4);
      _builder.append("    <p>");
      String _content = course.getContent();
      _builder.append(_content);
      _builder.append(" </p>");
      _builder.newLineIfNotEmpty();
      School2TextGenerator.operator_doubleLessThan(sBuilder, _builder);
      int _indentCount = this.indentCount;
      this.indentCount = (_indentCount + 2);
      this.generateCourseTabs(course.getCourseinstance(), sBuilder);
      this.indentCount--;
      StringConcatenation _builder_1 = new StringConcatenation();
      String _indent_5 = this.getIndent();
      _builder_1.append(_indent_5);
      _builder_1.append("</div>");
      _builder_1.newLineIfNotEmpty();
      School2TextGenerator.operator_doubleLessThan(sBuilder, _builder_1);
      this.generateCourseInfoSidebar(course, sBuilder);
      this.indentCount--;
      StringConcatenation _builder_2 = new StringConcatenation();
      String _indent_6 = this.getIndent();
      _builder_2.append(_indent_6);
      _builder_2.append("</div>");
      _builder_2.newLineIfNotEmpty();
      _xblockexpression = School2TextGenerator.operator_doubleLessThan(sBuilder, _builder_2);
    }
    return _xblockexpression;
  }
  
  public void generateCourseTabs(final EList<CourseInstance> instances, final StringBuilder builder) {
    this.indentCount++;
    StringConcatenation _builder = new StringConcatenation();
    String _indent = this.getIndent();
    _builder.append(_indent);
    _builder.append(".courseTab {");
    _builder.newLineIfNotEmpty();
    String _indent_1 = this.getIndent();
    _builder.append(_indent_1);
    _builder.append("    overflow: hidden;");
    _builder.newLineIfNotEmpty();
    String _indent_2 = this.getIndent();
    _builder.append(_indent_2);
    _builder.append("    border: 1px solid #ccc;");
    _builder.newLineIfNotEmpty();
    String _indent_3 = this.getIndent();
    _builder.append(_indent_3);
    _builder.append("    background-color: #f1f1f1;");
    _builder.newLineIfNotEmpty();
    String _indent_4 = this.getIndent();
    _builder.append(_indent_4);
    _builder.append("}");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    String _indent_5 = this.getIndent();
    _builder.append(_indent_5);
    _builder.append("/* Style the buttons that are used to open the tab content */");
    _builder.newLineIfNotEmpty();
    String _indent_6 = this.getIndent();
    _builder.append(_indent_6);
    _builder.append(".courseTab button {");
    _builder.newLineIfNotEmpty();
    String _indent_7 = this.getIndent();
    _builder.append(_indent_7);
    _builder.append("    background-color: inherit;");
    _builder.newLineIfNotEmpty();
    String _indent_8 = this.getIndent();
    _builder.append(_indent_8);
    _builder.append("    float: left;");
    _builder.newLineIfNotEmpty();
    String _indent_9 = this.getIndent();
    _builder.append(_indent_9);
    _builder.append("    border: none;");
    _builder.newLineIfNotEmpty();
    String _indent_10 = this.getIndent();
    _builder.append(_indent_10);
    _builder.append("    outline: none;");
    _builder.newLineIfNotEmpty();
    String _indent_11 = this.getIndent();
    _builder.append(_indent_11);
    _builder.append("    cursor: pointer;");
    _builder.newLineIfNotEmpty();
    String _indent_12 = this.getIndent();
    _builder.append(_indent_12);
    _builder.append("    padding: 14px 16px;");
    _builder.newLineIfNotEmpty();
    String _indent_13 = this.getIndent();
    _builder.append(_indent_13);
    _builder.append("    transition: 0.3s;");
    _builder.newLineIfNotEmpty();
    String _indent_14 = this.getIndent();
    _builder.append(_indent_14);
    _builder.append("}");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    String _indent_15 = this.getIndent();
    _builder.append(_indent_15);
    _builder.append("/* Change background color of buttons on hover */");
    _builder.newLineIfNotEmpty();
    String _indent_16 = this.getIndent();
    _builder.append(_indent_16);
    _builder.append(".courseTab button:hover {");
    _builder.newLineIfNotEmpty();
    String _indent_17 = this.getIndent();
    _builder.append(_indent_17);
    _builder.append("    background-color: #ddd;");
    _builder.newLineIfNotEmpty();
    String _indent_18 = this.getIndent();
    _builder.append(_indent_18);
    _builder.append("}");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    String _indent_19 = this.getIndent();
    _builder.append(_indent_19);
    _builder.append("/* Create an active/current tablink class */");
    _builder.newLineIfNotEmpty();
    String _indent_20 = this.getIndent();
    _builder.append(_indent_20);
    _builder.append(".courseTab button.active {");
    _builder.newLineIfNotEmpty();
    String _indent_21 = this.getIndent();
    _builder.append(_indent_21);
    _builder.append("    background-color: #ccc;");
    _builder.newLineIfNotEmpty();
    String _indent_22 = this.getIndent();
    _builder.append(_indent_22);
    _builder.append("}");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    String _indent_23 = this.getIndent();
    _builder.append(_indent_23);
    _builder.append("/* Style the tab content */");
    _builder.newLineIfNotEmpty();
    String _indent_24 = this.getIndent();
    _builder.append(_indent_24);
    _builder.append(".courseTabContent {");
    _builder.newLineIfNotEmpty();
    String _indent_25 = this.getIndent();
    _builder.append(_indent_25);
    _builder.append("    display: none;");
    _builder.newLineIfNotEmpty();
    String _indent_26 = this.getIndent();
    _builder.append(_indent_26);
    _builder.append("    padding: 6px 12px;");
    _builder.newLineIfNotEmpty();
    String _indent_27 = this.getIndent();
    _builder.append(_indent_27);
    _builder.append("    border: 1px solid #ccc;");
    _builder.newLineIfNotEmpty();
    String _indent_28 = this.getIndent();
    _builder.append(_indent_28);
    _builder.append("    border-top: none;");
    _builder.newLineIfNotEmpty();
    String _indent_29 = this.getIndent();
    _builder.append(_indent_29);
    _builder.append("} ");
    _builder.newLineIfNotEmpty();
    final String style = _builder.toString();
    StringConcatenation _builder_1 = new StringConcatenation();
    String _indent_30 = this.getIndent();
    _builder_1.append(_indent_30);
    _builder_1.append("function openInstance(evt, instanceId) {");
    _builder_1.newLineIfNotEmpty();
    String _indent_31 = this.getIndent();
    _builder_1.append(_indent_31);
    _builder_1.append("\t// Declare all variables");
    _builder_1.newLineIfNotEmpty();
    String _indent_32 = this.getIndent();
    _builder_1.append(_indent_32);
    _builder_1.append("\tvar i, tabcontent, tablinks;");
    _builder_1.newLineIfNotEmpty();
    _builder_1.newLine();
    String _indent_33 = this.getIndent();
    _builder_1.append(_indent_33);
    _builder_1.append("\t// Get all elements with class=\"courseTabContent\" and hide them");
    _builder_1.newLineIfNotEmpty();
    String _indent_34 = this.getIndent();
    _builder_1.append(_indent_34);
    _builder_1.append("\ttabcontent = document.getElementsByClassName(\"courseTabContent\");");
    _builder_1.newLineIfNotEmpty();
    String _indent_35 = this.getIndent();
    _builder_1.append(_indent_35);
    _builder_1.append("\tfor (i = 0; i < tabcontent.length; i++) {");
    _builder_1.newLineIfNotEmpty();
    String _indent_36 = this.getIndent();
    _builder_1.append(_indent_36);
    _builder_1.append("\t\ttabcontent[i].style.display = \"none\";");
    _builder_1.newLineIfNotEmpty();
    String _indent_37 = this.getIndent();
    _builder_1.append(_indent_37);
    _builder_1.append("\t}");
    _builder_1.newLineIfNotEmpty();
    _builder_1.newLine();
    String _indent_38 = this.getIndent();
    _builder_1.append(_indent_38);
    _builder_1.append("\t// Get all elements with class=\"tablinks\" and remove the class \"active\"");
    _builder_1.newLineIfNotEmpty();
    String _indent_39 = this.getIndent();
    _builder_1.append(_indent_39);
    _builder_1.append("\ttablinks = document.getElementsByClassName(\"tablinks\");");
    _builder_1.newLineIfNotEmpty();
    String _indent_40 = this.getIndent();
    _builder_1.append(_indent_40);
    _builder_1.append("\tfor (i = 0; i < tablinks.length; i++) {");
    _builder_1.newLineIfNotEmpty();
    String _indent_41 = this.getIndent();
    _builder_1.append(_indent_41);
    _builder_1.append("\t\ttablinks[i].className = tablinks[i].className.replace(\" active\", \"\");");
    _builder_1.newLineIfNotEmpty();
    String _indent_42 = this.getIndent();
    _builder_1.append(_indent_42);
    _builder_1.append("\t}");
    _builder_1.newLineIfNotEmpty();
    _builder_1.newLine();
    String _indent_43 = this.getIndent();
    _builder_1.append(_indent_43);
    _builder_1.append("\t// Show the current tab, and add an \"active\" class to the button that opened the tab");
    _builder_1.newLineIfNotEmpty();
    String _indent_44 = this.getIndent();
    _builder_1.append(_indent_44);
    _builder_1.append("\tdocument.getElementById(instanceId).style.display = \"block\";");
    _builder_1.newLineIfNotEmpty();
    String _indent_45 = this.getIndent();
    _builder_1.append(_indent_45);
    _builder_1.append("\tevt.currentTarget.className += \" active\";");
    _builder_1.newLineIfNotEmpty();
    String _indent_46 = this.getIndent();
    _builder_1.append(_indent_46);
    _builder_1.append("}");
    _builder_1.newLineIfNotEmpty();
    final String script = _builder_1.toString();
    this.indentCount--;
    this.generateStyle(style, builder);
    this.generateScript(script, builder);
    StringConcatenation _builder_2 = new StringConcatenation();
    String _indent_47 = this.getIndent();
    _builder_2.append(_indent_47);
    _builder_2.append("<div class=courseTab>");
    _builder_2.newLineIfNotEmpty();
    School2TextGenerator.operator_doubleLessThan(builder, _builder_2);
    this.indentCount++;
    final Consumer<CourseInstance> _function = (CourseInstance it) -> {
      this.generateCourseTabButtons(it, builder);
    };
    instances.forEach(_function);
    this.indentCount--;
    StringConcatenation _builder_3 = new StringConcatenation();
    String _indent_48 = this.getIndent();
    _builder_3.append(_indent_48);
    _builder_3.append("</div>");
    _builder_3.newLineIfNotEmpty();
    School2TextGenerator.operator_doubleLessThan(builder, _builder_3);
    final Consumer<CourseInstance> _function_1 = (CourseInstance it) -> {
      this.generateCourseTabContent(it, builder);
    };
    instances.forEach(_function_1);
  }
  
  public StringBuilder generateScript(final String script, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    String _indent = this.getIndent();
    _builder.append(_indent);
    _builder.append("<script>");
    _builder.newLineIfNotEmpty();
    _builder.append(script);
    _builder.newLineIfNotEmpty();
    String _indent_1 = this.getIndent();
    _builder.append(_indent_1);
    _builder.append("</script>");
    _builder.newLineIfNotEmpty();
    return School2TextGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  public StringBuilder generateStyle(final String style, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    String _indent = this.getIndent();
    _builder.append(_indent);
    _builder.append("<style>");
    _builder.newLineIfNotEmpty();
    _builder.append(style);
    _builder.newLineIfNotEmpty();
    String _indent_1 = this.getIndent();
    _builder.append(_indent_1);
    _builder.append("</style>");
    _builder.newLineIfNotEmpty();
    return School2TextGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  public StringBuilder generateCourseTabContent(final CourseInstance instance, final StringBuilder builder) {
    StringBuilder _xblockexpression = null;
    {
      String semesterType = "";
      int _value = instance.getSemesterType().getValue();
      boolean _equals = (_value == SemesterType.AUTUMN_VALUE);
      if (_equals) {
        semesterType = "H�st";
      } else {
        semesterType = "V�r";
      }
      StringConcatenation _builder = new StringConcatenation();
      String _indent = this.getIndent();
      _builder.append(_indent);
      _builder.append("<div id=");
      String _courseCode = instance.getCourse().getCourseCode();
      _builder.append(_courseCode);
      _builder.append(".");
      int _year = instance.getYear();
      _builder.append(_year);
      _builder.append(" class=courseTabContent>");
      _builder.newLineIfNotEmpty();
      String _indent_1 = this.getIndent();
      _builder.append(_indent_1);
      _builder.append("  <p>Semester: ");
      _builder.append(semesterType);
      _builder.append("</p>");
      _builder.newLineIfNotEmpty();
      String _indent_2 = this.getIndent();
      _builder.append(_indent_2);
      _builder.append("  <p>Poeng: ");
      double _credits = instance.getCredits();
      _builder.append(_credits);
      _builder.append("</p>");
      _builder.newLineIfNotEmpty();
      String _indent_3 = this.getIndent();
      _builder.append(_indent_3);
      _builder.append("  <p>Labtimer: ");
      int _totalLabHours = instance.getTimetable().getTotalLabHours();
      _builder.append(_totalLabHours);
      _builder.append("</p>");
      _builder.newLineIfNotEmpty();
      String _indent_4 = this.getIndent();
      _builder.append(_indent_4);
      _builder.append("  <p>Undervisningstimer: ");
      int _totalLecHours = instance.getTimetable().getTotalLecHours();
      _builder.append(_totalLecHours);
      _builder.append("</p>");
      _builder.newLineIfNotEmpty();
      String _indent_5 = this.getIndent();
      _builder.append(_indent_5);
      _builder.append("</div>");
      _builder.newLineIfNotEmpty();
      _xblockexpression = School2TextGenerator.operator_doubleLessThan(builder, _builder);
    }
    return _xblockexpression;
  }
  
  public StringBuilder generateCourseTabButtons(final CourseInstance instance, final StringBuilder builder) {
    StringConcatenation _builder = new StringConcatenation();
    String _indent = this.getIndent();
    _builder.append(_indent);
    _builder.append("<button class=\"tablinks\" onclick=\"openInstance(event, \'");
    String _courseCode = instance.getCourse().getCourseCode();
    _builder.append(_courseCode);
    _builder.append(".");
    int _year = instance.getYear();
    _builder.append(_year);
    _builder.append("\')\">");
    int _year_1 = instance.getYear();
    _builder.append(_year_1);
    _builder.append("</button>");
    _builder.newLineIfNotEmpty();
    return School2TextGenerator.operator_doubleLessThan(builder, _builder);
  }
  
  public StringBuilder generateCourseInfoSidebar(final Course course, final StringBuilder sBuilder) {
    StringBuilder _xblockexpression = null;
    {
      StringConcatenation _builder = new StringConcatenation();
      String _indent = this.getIndent();
      _builder.append(_indent);
      _builder.append("<div class=\"courseSideBar\">");
      _builder.newLineIfNotEmpty();
      String _indent_1 = this.getIndent();
      _builder.append(_indent_1);
      _builder.append("  <h4>Studielinjer:</h4>");
      _builder.newLineIfNotEmpty();
      String _indent_2 = this.getIndent();
      _builder.append(_indent_2);
      _builder.append("  <ul>");
      _builder.newLineIfNotEmpty();
      School2TextGenerator.operator_doubleLessThan(sBuilder, _builder);
      int _indentCount = this.indentCount;
      this.indentCount = (_indentCount + 2);
      final Consumer<StudyProgram> _function = (StudyProgram it) -> {
        this.generateCourseStudyPrograms(it, sBuilder);
      };
      course.getPartOf().forEach(_function);
      int _indentCount_1 = this.indentCount;
      this.indentCount = (_indentCount_1 - 2);
      StringConcatenation _builder_1 = new StringConcatenation();
      String _indent_3 = this.getIndent();
      _builder_1.append(_indent_3);
      _builder_1.append("  </ul>");
      _builder_1.newLineIfNotEmpty();
      School2TextGenerator.operator_doubleLessThan(sBuilder, _builder_1);
      int _size = course.getRequiresCourse().size();
      boolean _greaterThan = (_size > 0);
      if (_greaterThan) {
        StringConcatenation _builder_2 = new StringConcatenation();
        String _indent_4 = this.getIndent();
        _builder_2.append(_indent_4);
        _builder_2.append("  <h4>Krever fag:</h4>");
        _builder_2.newLineIfNotEmpty();
        String _indent_5 = this.getIndent();
        _builder_2.append(_indent_5);
        _builder_2.append("  <ul>");
        _builder_2.newLineIfNotEmpty();
        School2TextGenerator.operator_doubleLessThan(sBuilder, _builder_2);
        int _indentCount_2 = this.indentCount;
        this.indentCount = (_indentCount_2 + 2);
        final Consumer<Course> _function_1 = (Course it) -> {
          this.generateRequiredCourses(it, sBuilder);
        };
        course.getRequiresCourse().forEach(_function_1);
        int _indentCount_3 = this.indentCount;
        this.indentCount = (_indentCount_3 - 2);
        StringConcatenation _builder_3 = new StringConcatenation();
        String _indent_6 = this.getIndent();
        _builder_3.append(_indent_6);
        _builder_3.append("  </ul>");
        _builder_3.newLineIfNotEmpty();
        School2TextGenerator.operator_doubleLessThan(sBuilder, _builder_3);
      }
      StringConcatenation _builder_4 = new StringConcatenation();
      String _indent_7 = this.getIndent();
      _builder_4.append(_indent_7);
      _builder_4.append("</div>");
      _builder_4.newLineIfNotEmpty();
      _xblockexpression = School2TextGenerator.operator_doubleLessThan(sBuilder, _builder_4);
    }
    return _xblockexpression;
  }
  
  public StringBuilder generateRequiredCourses(final Course course, final StringBuilder sBuilder) {
    StringConcatenation _builder = new StringConcatenation();
    String _indent = this.getIndent();
    _builder.append(_indent);
    _builder.append("<li>");
    String _courseCode = course.getCourseCode();
    _builder.append(_courseCode);
    _builder.append(" - ");
    String _name = course.getName();
    _builder.append(_name);
    _builder.append("</li>");
    _builder.newLineIfNotEmpty();
    return School2TextGenerator.operator_doubleLessThan(sBuilder, _builder);
  }
  
  public StringBuilder generateCourseStudyPrograms(final StudyProgram program, final StringBuilder sBuilder) {
    StringConcatenation _builder = new StringConcatenation();
    String _indent = this.getIndent();
    _builder.append(_indent);
    _builder.append("<li>");
    String _code = program.getCode();
    _builder.append(_code);
    _builder.append(" - ");
    String _name = program.getName();
    _builder.append(_name);
    _builder.append("</li>");
    _builder.newLineIfNotEmpty();
    return School2TextGenerator.operator_doubleLessThan(sBuilder, _builder);
  }
  
  /**
   * ��
   */
  public StringBuilder genereateHeader(final String headerText, final int headerSize, final StringBuilder sBuilder) {
    StringBuilder _xblockexpression = null;
    {
      final String indent = this.getIndent();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(indent);
      _builder.append("<h");
      _builder.append(headerSize);
      _builder.append(">");
      _builder.append(headerText);
      _builder.append("</h");
      _builder.append(headerSize);
      _builder.append(">");
      _builder.newLineIfNotEmpty();
      _xblockexpression = School2TextGenerator.operator_doubleLessThan(sBuilder, _builder);
    }
    return _xblockexpression;
  }
  
  public CharSequence applyPreHtml(final String title, final StringBuilder sBuilder) {
    StringBuilder _xblockexpression = null;
    {
      int _indentCount = this.indentCount;
      this.indentCount = (_indentCount + 2);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<html>");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("<head>");
      _builder.newLine();
      _builder.append("    ");
      _builder.append("<title>");
      _builder.append(title, "    ");
      _builder.append("</title>");
      _builder.newLineIfNotEmpty();
      _builder.append("    ");
      _builder.append("<meta charset=\"utf-8\"/>");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("</head>");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("<body>");
      _builder.newLine();
      _xblockexpression = School2TextGenerator.operator_doubleLessThan(sBuilder, _builder);
    }
    return _xblockexpression;
  }
  
  public CharSequence applyPreHtml(final String title, final StringBuilder sBuilder, final String style) {
    StringBuilder _xblockexpression = null;
    {
      int _indentCount = this.indentCount;
      this.indentCount = (_indentCount + 2);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<html>");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("<head>");
      _builder.newLine();
      _builder.append("    ");
      _builder.append("<title>");
      _builder.append(title, "    ");
      _builder.append("</title>");
      _builder.newLineIfNotEmpty();
      _builder.append("    ");
      _builder.append("<meta charset=\"utf-8\"/>");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("</head>");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("<style>");
      _builder.append(style, "  ");
      _builder.append("</style>");
      _builder.newLineIfNotEmpty();
      _builder.append("  ");
      _builder.append("<body>");
      _builder.newLine();
      _xblockexpression = School2TextGenerator.operator_doubleLessThan(sBuilder, _builder);
    }
    return _xblockexpression;
  }
  
  public CharSequence applyPreHtml(final String title, final StringBuilder sBuilder, final String style, final String script) {
    StringBuilder _xblockexpression = null;
    {
      int _indentCount = this.indentCount;
      this.indentCount = (_indentCount + 2);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<html>");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("<head>");
      _builder.newLine();
      _builder.append("    ");
      _builder.append("<title>");
      _builder.append(title, "    ");
      _builder.append("</title>");
      _builder.newLineIfNotEmpty();
      _builder.append("    ");
      _builder.append("<meta charset=\"utf-8\"/>");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("</head>");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("<style>");
      _builder.append(style, "  ");
      _builder.append("</style>");
      _builder.newLineIfNotEmpty();
      _builder.append("  ");
      _builder.append("<script>");
      _builder.append(script, "  ");
      _builder.append("</script>");
      _builder.newLineIfNotEmpty();
      _builder.append("  ");
      _builder.append("<body>");
      _builder.newLine();
      _xblockexpression = School2TextGenerator.operator_doubleLessThan(sBuilder, _builder);
    }
    return _xblockexpression;
  }
  
  public CharSequence applyPostHtml(final StringBuilder sBuilder) {
    StringBuilder _xblockexpression = null;
    {
      int _indentCount = this.indentCount;
      this.indentCount = (_indentCount - 2);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("  ");
      _builder.append("</body>");
      _builder.newLine();
      _builder.append("</html>");
      _builder.newLine();
      _xblockexpression = School2TextGenerator.operator_doubleLessThan(sBuilder, _builder);
    }
    return _xblockexpression;
  }
  
  public static StringBuilder operator_doubleLessThan(final StringBuilder stringBuilder, final Object o) {
    return stringBuilder.append(o);
  }
}

# TDT4250 Indivdual modelling assignment

#### Classes
- **School:**
	Main root of the model. Constainer for **Course**, **Person**, **Department**, **Rooms** and **StudyProgram** objects.
- **Course**:
	Container for **CourseInstance** objects, and has standard information about a course
- **Department**: 
	Part of the school that manages various course instances
- **StudyProgram**:
	A program of study students get registered to. Consists of a collection of courses
- **Person**:
	A person that exists in the school. Has a name and an ID, and holds several different **roles**
- **CourseInstance**:
	Container for **Timetable**, **Role**, and **EvaluationForm** objects. Holds information specific to a specific semester
- **Role:** 
	A **person**'s connection to a subject
- **Timetable**: 
	Container for **Reservation** objects. Provides a list of various room-reservation a course has
- **Reservation**: 
	A class holding information about when a room is reserved
- **EvaluationForm**:
	A description of how an instance of a course will be graded. Contains **Activity** objects
- **Activity**:
	Superclass for **Assignment**, **Exam**, and **Project** classes.
- **Project**:
	A form of **Activity**
- **Exam**: 
	A form of **Activity**
- **Assignment**:
	A form of **Activity**
- **Roletype:**
	*Enum* with different roles a person can have in various courses. Used by **Roles**.
- **Day:**
	*Enum* with the different days of the week. Used by **Reservation**.
- **Semestertype:**
	*Enum* differentiating between Spring and Autumn semesters. Used by **CourseInstance**
- **ReservationType:**
	*Enum* of the different types of purposes a room is reserved for. Used by **Reservation**
	
### HTML generator
To generate a html based on a given model instance, run the class School2TextGeneratorMain as a java application. It will use the SampleSchool.xmi file if no arguments are given, and the generated output is saved as the file **school.html**

	